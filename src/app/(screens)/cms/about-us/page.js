import Header from '@/components/header'
import CmsPageComponent from '@/components/pages/cmsPages/otherComponents/cmsPageComponents'
import React from 'react'


const page = () => {


    return (
        <>
            <Header title='About Us' />
            <CmsPageComponent title='About us INFORMATION' />
        </>
    )
}

export default page