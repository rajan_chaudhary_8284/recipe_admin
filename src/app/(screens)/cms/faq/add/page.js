import Header from '@/components/header'
import AddFaqComponent from '@/components/pages/cmsPages/faq/addFaqComponent'
import React from 'react'

const page = () => {


    return (
        <>
            <Header title='Add Faq' />
            <AddFaqComponent />
        </>
    )
}

export default page