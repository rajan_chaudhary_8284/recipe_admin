import Header from '@/components/header'
import FaqListComponent from '@/components/pages/cmsPages/faq/faqListComponent'
import React from 'react'

const page = () => {

    return (
        <>
            <Header title='Faq List' />
            <FaqListComponent />
        </>
    )
}

export default page