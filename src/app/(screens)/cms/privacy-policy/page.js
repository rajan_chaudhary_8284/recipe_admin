import Header from '@/components/header'
import CmsPageComponent from '@/components/pages/cmsPages/otherComponents/cmsPageComponents'
import React from 'react'


const page = () => {

    return (
        <>
            <Header title='Privacy Policy' />
            <CmsPageComponent title='Privacy Policy INFORMATION' />
        </>
    )
}

export default page