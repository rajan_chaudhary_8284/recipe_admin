import Header from '@/components/header'
import CmsPageComponent from '@/components/pages/cmsPages/otherComponents/cmsPageComponents'
import React from 'react'


const page = () => {

    return (
        <>
            <Header title='Terms & Conditions' />
            <CmsPageComponent title='Terms & Conditions INFORMATION' />
        </>
    )
}

export default page