import Header from '@/components/header'
import React from 'react'
import ContactUsDetailComponent from '@/components/pages/contactUs/contactUsDetailComponent';


const page = () => {


    return (
        <>
            <Header title='Contact Us Details' />
            <ContactUsDetailComponent />
        </>
    )
}

export default page