import Header from '@/components/header'
import ContactUsListComponent from '@/components/pages/contactUs/contactUsListComponent';
import React from 'react'


const page = () => {

    return (
        <>
            <Header title='Contact Us List' />
            <ContactUsListComponent />
        </>
    )
};

export default page