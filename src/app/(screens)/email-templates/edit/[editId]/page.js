import Header from '@/components/header'
import EditEmailTemplateComponent from '@/components/pages/emailTemplates/editEmailTemplate'
import React from 'react'


const page = () => {


    return (
        <>
            <Header title='Edit Email Template' />
            <EditEmailTemplateComponent />
        </>
    )
}

export default page