import Header from '@/components/header'
import EmailTemplateListComponent from '@/components/pages/emailTemplates/emailTemplateList'
import React from 'react'

const page = () => {

    return (
        <>
            <Header title='Email Templates List' />
            <EmailTemplateListComponent />
        </>
    )
}

export default page