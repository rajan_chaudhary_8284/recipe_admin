import MainLayout from "@/components/Layouts/mainLayout";


const Layout = ({ children }) => {

    return (
        <MainLayout>
            {children}
        </MainLayout>
    );
};

export default Layout;