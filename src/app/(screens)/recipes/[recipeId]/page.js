import Header from '@/components/header'
import RecipeDetailComponent from '@/components/pages/recipes/recipeDetailComponent'



const page = () => {


    return (
        <>
            <Header title='Recipe Details' />
            <RecipeDetailComponent />
        </>
    )
}

export default page