import Header from '@/components/header';
import AddRecipeComponent from '@/components/pages/recipes/addRecipeComponent';
import React from 'react'


const page = () => {

    return (
        <>
            <Header title='Add Recipe' />
            <AddRecipeComponent />
        </>
    )
};

export default page