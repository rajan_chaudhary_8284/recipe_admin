import Header from '@/components/header';
import AddProductComponent from '@/components/pages/recipes/addRecipeComponent';
import React from 'react'


const page = () => {


    return (
        <>
            <Header title='Edit Product' />
            <AddProductComponent />
        </>
    )
};

export default page