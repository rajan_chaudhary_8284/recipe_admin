import Header from '@/components/header';
import RecipeListComponent from '@/components/pages/recipes/recipesListComponent';



const page = () => {


    return (
        <>
            <Header title='Recipe List' />
            <RecipeListComponent />
        </>
    )
};

export default page