import Header from '@/components/header';
import UserListScreen from '@/components/pages/user-list';
import React from 'react'


const page = () => {

    return (
        <>
            <Header title='Users List' />
            <UserListScreen />
        </>
    )
};

export default page