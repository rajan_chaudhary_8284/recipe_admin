import SideBar from '@/components/sideBar';
import styles from '../../../../public/scss/layout.module.scss'

const MainLayout = ({ children }) => {


    return (
        <div className={styles.layoutMain}>
            <div className={styles.row}>
                <div className={styles.left}>
                    <SideBar />
                </div>
                <div className={styles.right}>
                    {children}
                    {/* <div className={styles.poweredBy}>
                        <div className={styles.text}>Powered By <a href="https://globiztechnology.com/" target='blank'> Globiz Technology Inc.</a></div>
                    </div> */}
                </div>

            </div>
        </div>
    )
};


export default MainLayout