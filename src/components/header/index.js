'use client';
import React from 'react'
import styles from '../../../public/scss/pages/header.module.scss';
import { Button, Col, Form, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBell, faCartShopping, faCirclePlus, faEnvelope, faMagnifyingGlass } from '@fortawesome/free-solid-svg-icons';
import user from '../../../public/images/userDummy.png';
import Image from 'next/image';

const Header = (props) => {

    const { title, showBtn } = props;

    return (
        <div className={styles.main}>
            <Row style={{ alignItems: 'center' }}>
                <Col xxl={7} xl={7} lg={7} md={7} sm={7} xs={7} >
                    <p className={styles.title}>{title ? title : null}</p>
                </Col>
                <Col xxl={5} xl={5} lg={5} md={5} sm={5} xs={5}>
                    <div className={styles.rightSide}>
                        <div className={styles.nofification}>
                            <FontAwesomeIcon icon={faBell} className={styles.icon} />
                            <div className={styles.countMain}>
                                <p className={styles.count}>10</p>
                            </div>
                        </div>
                        {/* <div className={styles.nofification}>
                            <FontAwesomeIcon icon={faCartShopping} className={styles.icon} />
                            <div className={styles.countMain}>
                                <p className={styles.count}>10</p>
                            </div>
                        </div> */}
                        <div className={styles.imgProfile}>
                            <Image
                                src={user}
                                className={styles.image}
                                alt='image'
                            />
                        </div>
                    </div>
                </Col>
            </Row>
        </div>
    )
}

export default Header