"use client"
import React, { useEffect, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import styles from '../../../public/scss/pages/confirmationModal.module.scss';
import { Button, Form, Modal } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClose, faTrashCan } from '@fortawesome/free-solid-svg-icons';
import Image from 'next/image';
import deleteImg from '../../../public/images/delete.png';

export default function DeleteConfirmationModal(props) {
    const { show, onClose, message, handleSubmit } = props
    // const [domLoaded, setDomLoaded] = useState(false);

    // useEffect(() => {
    //     setDomLoaded(true);
    // }, []);

    // const handleSubmit = () => {
    //     props.handleSubmit();
    // };

    return (
        <Modal
            show={props.show}
            onHide={props.close}
            centered
            className={styles.modal}
        >
            <div className={styles.main}>
                <div className={styles.IconMain}>
                    <Image
                        src={deleteImg}
                        className={styles.image}
                        alt='image'
                    />
                    {/* <FontAwesomeIcon
                        icon={faTrashCan}
                        className={styles.deletIcon}
                    /> */}
                </div>
                <div className={styles.message}>
                    {props && props.message ? props.message : 'Do you wish to delete this Product ?'}
                </div>
                <div className={styles.buttonMain}>
                    <Button className={styles.closeButton} onClick={() => onClose()}>
                        Cancel
                    </Button>
                    <Button className={styles.submitButton} onClick={() => handleSubmit()}>
                        Submit
                    </Button>
                </div>
                <div className={styles.closeIconAbsulate} onClick={() => onClose()}>
                    <FontAwesomeIcon
                        icon={faClose}
                        className={styles.icon}
                    />
                </div>
            </div>
        </Modal>
    );
}