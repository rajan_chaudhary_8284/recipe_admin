import React, { useState } from 'react'
import styles from '../../../../../public/scss/pages/forgotPassword.module.scss';
import Image from 'next/image';
import logo from '../../../../../public/images/lock.png';
import { Button, Col, Form, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import Otp from '../otpScreen';

const ForgotPassword = (props) => {
    const [activeIcon, setActiveIcon] = useState(false);
    const [otpScreen, setOtpScreen] = useState(false);

    return (
        <>
            {otpScreen ?
                <Otp />
                :

                <div className={styles.main}>
                    <div className={styles.imgMain}>
                        <Image
                            src={logo}
                            className={styles.image}
                            alt='image'
                        />
                    </div>
                    <p className={styles.title}>Forgot Your Password?</p>
                    <p className={styles.disTxt}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                        incididunt ut labore et dolore magna aliqua.
                    </p>
                    <div className={styles.form}>
                        <Form>
                            <Form.Group controlId="formBasicEmail" className={styles.form_group}>
                                <Form.Label className={styles.label}>Email</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="Enter your email"
                                    onFocus={() => setActiveIcon(true)}
                                    onBlur={() => setActiveIcon(false)}
                                />
                                <div className={styles.iconView}>
                                    <FontAwesomeIcon icon={faEnvelope} className={activeIcon ? styles.iconActive : styles.icon} />
                                </div>
                                {/* <p className='error-msg'>{'This field is required'}</p> */}
                            </Form.Group>
                            <div className={styles.viewBtn}>
                                <Row>
                                    <Col xxl={6} xl={6} lg={6} md={6} sm={12} xs={12} >
                                        <Button type="submit"
                                            className={styles.btnBack}
                                            onClick={() => close()}
                                        >
                                            Back
                                        </Button>
                                    </Col>
                                    <Col xxl={6} xl={6} lg={6} md={6} sm={12} xs={12} >
                                        <Button type="submit"
                                            className={styles.btn}
                                            onClick={() => setOtpScreen(true)}
                                        >
                                            Submit
                                        </Button>
                                    </Col>
                                </Row>
                            </div>
                        </Form>
                    </div>
                </div>
            }
        </>
    )
};

export default ForgotPassword