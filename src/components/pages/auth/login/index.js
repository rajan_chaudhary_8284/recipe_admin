"use client"
import { faEnvelope, faEye, faEyeSlash, faLock } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Image from 'next/image';
import { useRouter } from 'next/navigation';
import { useState } from 'react';
import { Button, Col, Form, Row } from 'react-bootstrap';
import forgotImg from '../../../../../public/images/forgotImg.svg';
import loginImg from '../../../../../public/images/loginImg.jpg';
import logo from '../../../../../public/images/logo.png';
import styles from '../../../../../public/scss/pages/login.module.scss';
import ForgotPassword from '../forgotPassword';


const Login = () => {
    const [activeIcon, setActiveIcon] = useState(false);
    const [activeIcon1, setActiveIcon1] = useState(false);
    const [isVisiblePassword, setVisiblePassword] = useState(false);
    const [ForgotPasswordComp, setForgotPasswordComp] = useState(false);
    const route = useRouter();


    return (
        <div className={styles.main}>
            <div className={styles.mainContainer}>
                <div className={styles.slop} >
                </div>
                    <div className={styles.imgLogo}>
                        <div className={styles.imgMain}>
                            <Image
                                src={logo}
                                className={styles.image}
                                alt='image'
                                priority={true}
                            />
                        </div>
                    </div>
                <div className={styles.rightArea}>
                    <h2 className={styles.welcome}>Welcome Back!</h2>
                    <p className={styles.loginText}>Please Login to your account</p>
                    <div className={styles.form}>
                        <Form onSubmit={(e) => (e.preventDefault(), route.push('/recipes'))}>
                            <Form.Group controlId="formBasicEmail" className={styles.form_group}>
                                <Form.Label className={styles.label}>Email</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="Enter your email"
                                    onFocus={() => setActiveIcon(true)}
                                    onBlur={() => setActiveIcon(false)}
                                />
                                <div className={styles.iconView}>
                                    <FontAwesomeIcon icon={faEnvelope} className={activeIcon ? styles.iconActive : styles.icon} />
                                </div>

                                {/* <p className='error-msg'>{'This field is required'}</p> */}

                            </Form.Group>
                            <Form.Group className={styles.form_group} controlId="formBasicPassword">
                                <Form.Label className={styles.label}>Password</Form.Label>
                                <Form.Control
                                    placeholder="Enter your password"
                                    type={!isVisiblePassword ? "password" : "text"}
                                    onFocus={() => setActiveIcon1(true)}
                                    onBlur={() => setActiveIcon1(false)}
                                />
                                <div className={styles.iconView}>
                                    <FontAwesomeIcon icon={faLock} className={activeIcon1 ? styles.iconActive : styles.icon} />
                                </div>
                                <span onClick={() => setVisiblePassword(!isVisiblePassword)} className={styles.iconEye}>
                                    {isVisiblePassword ?
                                        <FontAwesomeIcon
                                            className={styles.icon}
                                            icon={faEye}
                                        />
                                        : <FontAwesomeIcon
                                            className={styles.icon}
                                            icon={faEyeSlash}
                                        />}
                                </span>
                                {/* <p className='error-msg'>{'This field is required'}</p> */}
                            </Form.Group>
                            <div className={styles.checkBoxMain} >
                                {/* <Form.Check
                                                                type="switch"
                                                                id="disabled-custom-switch"
                                                                label={'Remember me'}
                                                            // checked={true}
                                                            // onChange={(e) => handleRemember(e.target.checked)}
                                                            /> */}
                                {/* <p className={styles.forgot} onClick={() => setForgotPasswordComp(true)}>Forgot Password?</p> */}
                            </div>

                            <div className={styles.viewBtn}>
                                <Button type="submit"
                                    className={styles.btn}
                                >
                                    Login
                                </Button>
                            </div>
                            <p className={styles.years}>© 2023 <span style={{ cursor: "pointer" }}>Globiz Technology Inc.</span></p>
                        </Form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login