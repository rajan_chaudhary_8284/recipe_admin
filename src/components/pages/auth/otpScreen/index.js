import React, { useState } from 'react'
import Image from 'next/image';
import styles from '../../../../../public/scss/pages/otp.module.scss';
import logo from '../../../../../public/images/lockPin.png';
import { Button, Form } from 'react-bootstrap';
import OTPInput from 'react-otp-input';
import ResetPassword from '../resetPassword';

const Otp = () => {
    const [otp, setOtp] = useState(null);
    const [resetPasswordScreen, setResetPasswordScreen] = useState(false);

    return (
        <>
            {
                resetPasswordScreen ?
                    <ResetPassword />
                    :
                    <div className={styles.main}>
                        <div className={styles.imgMain}>
                            <Image
                                src={logo}
                                className={styles.image}
                                alt='image'
                            />
                        </div>
                        <p className={styles.title}>OTP Verification</p>
                        <p className={styles.disTxt}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                            incididunt ut labore et dolore magna aliqua.
                        </p>
                        <div className={styles.form}>
                            <Form>
                                <Form.Group controlId="formBasicEmail" className={styles.form_group}>
                                    {/* <Form.Label className={styles.label}>Otp</Form.Label> */}
                                    <OTPInput
                                        value={otp}
                                        onChange={setOtp}
                                        numInputs={6}
                                        renderInput={(props) => <input {...props} />}
                                        containerStyle={{
                                            justifyContent: 'space-between',
                                            paddingTop: '10px'
                                        }}
                                        inputStyle={styles.inputStyle}
                                        type={'number'}
                                        shouldAutoFocus={true}
                                    />
                                    {/* <p className='error-msg'>{'This field is required'}</p> */}
                                </Form.Group>
                                <p className={styles.resendOtp}>Resend OTP?</p>
                                <div className={styles.viewBtn}>
                                    <Button type="submit"
                                        className={styles.btn}
                                        onClick={() => setResetPasswordScreen(true)}
                                    >
                                        Verify
                                    </Button>
                                </div>
                            </Form>
                        </div>
                    </div>
            }
        </>
    )
}

export default Otp