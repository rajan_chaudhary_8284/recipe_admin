import React, { useState } from 'react'
import styles from '../../../../../public/scss/pages/resetPassword.module.scss';
import Image from 'next/image';
import logo from '../../../../../public/images/lockReset.png';
import { Button, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLock } from '@fortawesome/free-solid-svg-icons';


const ResetPassword = () => {
    const [activeIcon, setActiveIcon] = useState(false);
    const [activeIcon1, setActiveIcon1] = useState(false);

    return (
        <div className={styles.main}>
            <div className={styles.imgMain}>
                <Image
                    src={logo}
                    className={styles.image}
                    alt='image'
                />
            </div>
            <p className={styles.title}>Reset Your Password</p>
            <p className={styles.disTxt}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                incididunt ut labore et dolore magna aliqua.
            </p>
            <div className={styles.form}>
                <Form>
                    <Form.Group controlId="formBasicEmail" className={styles.form_group}>
                        <Form.Label className={styles.label}>New Password</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Enter your new password"
                            onFocus={() => setActiveIcon(true)}
                            onBlur={() => setActiveIcon(false)}
                        />
                        <div className={styles.iconView}>
                            <FontAwesomeIcon icon={faLock} className={activeIcon ? styles.iconActive : styles.icon} />
                        </div>
                        {/* <p className='error-msg'>{'This field is required'}</p> */}
                    </Form.Group>
                    <Form.Group controlId="formBasicEmail" className={styles.form_group}>
                        <Form.Label className={styles.label}>Confirm Password</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Enter your confirm password"
                            onFocus={() => setActiveIcon1(true)}
                            onBlur={() => setActiveIcon1(false)}
                        />
                        <div className={styles.iconView}>
                            <FontAwesomeIcon icon={faLock} className={activeIcon1 ? styles.iconActive : styles.icon} />
                        </div>
                        {/* <p className='error-msg'>{'This field is required'}</p> */}
                    </Form.Group>
                    <div className={styles.viewBtn}>
                        <Button type="submit"
                            className={styles.btn}
                            onClick={() => setOtpScreen(true)}
                        >
                            Submit
                        </Button>
                    </div>
                </Form>
            </div>
        </div>
    )
};

export default ResetPassword