"use client";
import styles from '../../../../../public/scss/pages/addFaq.module.scss';
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { useRouter } from 'next/navigation';


const AddFaqComponent = (props) => {
    const route = useRouter();


    return (
        <div className={styles.main}>
            <div className={styles.header}>
                <p className={styles.heading}>Faq Information</p>
            </div>
            <div className={styles.formArea}>
                <Container fluid>
                    <Row>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Form.Group className={styles.form_group}>
                                <Form.Label className={'form-label'}>Title<span>*</span></Form.Label>
                                <Form.Control
                                    className={styles.form_control}
                                    type="text"
                                    placeholder="Enter title here..."
                                />
                                {/* <p className={'error-msg'}>{'This field is required'}</p> */}
                            </Form.Group>
                        </Col>

                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Form.Group className={styles.form_group}>
                                <Form.Label className={'form-label'}>Description<span>*</span></Form.Label>
                                <textarea
                                    className={styles.textArea}
                                    placeholder='Enter description here...'
                                // value={values && values.answer}
                                // onInput={(e) => validate(e.target)}
                                // onChange={(e) => handleChange('answer', e.target.value)}
                                />
                                {/* <p className={'error-msg'}>{'This field is required'}</p> */}
                            </Form.Group>
                        </Col>

                        <div className={styles.viewBtn}>
                            <Button type="submit" className={styles.btn}>Submit</Button>
                        </div>
                    </Row>
                </Container>
            </div>
        </div>
    )
}

export default AddFaqComponent