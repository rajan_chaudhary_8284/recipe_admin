"use client";
import styles from '../../../../../public/scss/pages/cmsPageComponent.module.scss';
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import { useRouter } from 'next/navigation';
import dynamic from 'next/dynamic';
import { faArrowUpFromBracket, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


const CmsPageComponent = (props) => {
    const route = useRouter();
    const {
        title
    } = props;

    const TextEditor = dynamic(() => import('../../../uiComponents/textEditorComponent'))

    return (
        <div className={styles.main}>
            <div className={styles.header}>
                <p className={styles.heading}>{title ? title : null}</p>
            </div>
            <div className={styles.formArea}>
                <Container fluid>
                    <Row>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Form.Group className={styles.form_group}>
                                <Form.Label className={'form-label'}>Title<span>*</span></Form.Label>
                                <Form.Control
                                    className={styles.form_control}
                                    type="text"
                                    placeholder="Enter title here..."
                                />
                                {/* <p className={'error-msg'}>{'This field is required'}</p> */}
                            </Form.Group>
                        </Col>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Form.Group className={styles.form_group}>
                                <Form.Label className={'form-label'}>Description<span>*</span></Form.Label>
                                <TextEditor
                                // data={values && values.description}
                                // onChange={(data) => handleChange('description', data)}
                                />
                                {/* <p className={'error-msg'}>{'This field is required'}</p> */}
                            </Form.Group>
                        </Col>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <div className={styles.line} />
                            <p className={styles.metaTitle}>SEO META INFORMATION</p>
                        </Col>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Form.Group className={styles.form_group}>
                                <Form.Label className={'form-label'}>Meta Title<span>*</span></Form.Label>
                                <Form.Control
                                    className={styles.form_control}
                                    type="text"
                                    placeholder="Enter meta title here..."
                                />
                                {/* <p className={'error-msg'}>{'This field is required'}</p> */}
                            </Form.Group>
                        </Col>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Form.Group className={styles.form_group}>
                                <Form.Label className={'form-label'}>Meta Description <span>*</span></Form.Label>
                                <textarea
                                    className={styles.textArea}
                                    placeholder='Enter meta description here...'
                                // value={values && values.answer}
                                // onInput={(e) => validate(e.target)}
                                // onChange={(e) => handleChange('answer', e.target.value)}
                                />
                                {/* <p className={'error-msg'}>{'This field is required'}</p> */}
                            </Form.Group>
                        </Col>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Form.Group className={styles.form_group}>
                                <Form.Label className={'form-label'}>Meta Keyboard<span>*</span></Form.Label>
                                <Form.Control
                                    className={styles.form_control}
                                    type="text"
                                    placeholder="Enter meta keyboard here..."
                                />
                                {/* <p className={'error-msg'}>{'This field is required'}</p> */}
                            </Form.Group>
                        </Col>
                        <div className={styles.uploadbtn}>
                            <Button type="submit" className={styles.btnU} >
                                <FontAwesomeIcon icon={faArrowUpFromBracket} className={styles.icon} />
                                Upload Image
                            </Button>
                        </div>
                        <p className={styles.recomendedSize}>Recomended Size: 1080 X 640 px</p>
                        <div className={styles.viewBtn}>
                            <Button type="submit" className={styles.btn}>Submit</Button>
                        </div>
                    </Row>
                </Container>
            </div>
        </div>
    )
}

export default CmsPageComponent