"use client"
import React from 'react'
import styles from '../../../../public/scss/pages/faqDetails.module.scss'


const ContactUsDetailComponent = () => {


    return (
        <div className={styles.main}>
            <div className={styles.header}>
                <p className={styles.heading}>Contact Us Information</p>
            </div>
            <div className={styles.infoMain}>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Id:</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>1</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Title:</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Status:</p>
                    </div>
                    <div className={styles.col2}>
                        <div className={styles.tag}>
                            {/* <p className={styles.inactive}>Inactive</p> */}
                            <p className={styles.active}>Active</p>
                        </div>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Created On:</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>22-12-2024 12:10AM</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Last modified:</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>22-12-2024 12:10AM</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Message:</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ContactUsDetailComponent