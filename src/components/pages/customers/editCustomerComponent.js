"use client"
import React, { useState } from 'react'
import styles from '../../../../public/scss/pages/addVendor.module.scss';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAddressCard, faEnvelope, faLocationDot, faLock, faPhone, faRectangleList, faUser } from '@fortawesome/free-solid-svg-icons';
import UploadImageBox from '@/helpers/uploadUserImage';

const EditCustomerComponent = () => {
    const [activeIcon, setActiveIcon] = useState(false);
    const [activeIcon2, setActiveIcon2] = useState(false);
    const [activeIcon3, setActiveIcon3] = useState(false);
    const [activeIcon4, setActiveIcon4] = useState(false);
    const [activeIcon5, setActiveIcon5] = useState(false);
    const [activeIcon6, setActiveIcon6] = useState(false);
    const [activeIcon7, setActiveIcon7] = useState(false);
    const [activeIcon8, setActiveIcon8] = useState(false);
    const [activeIcon9, setActiveIcon9] = useState(false);
    const [activeIcon10, setActiveIcon10] = useState(false);


    return (
        <div className={styles.main}>
            <div className={styles.formArea}>
                <div className={styles.viewImage}>
                    <UploadImageBox
                        name={``}
                        // image={values && values.image}
                        // onResponse={(response) => {
                        //     handleChange("image", response.path)
                        // }}
                        removeImage={async () => {
                            //  await removeImage();
                        }}
                    />
                </div>
                <Container fluid>
                    <Row>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Row>
                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                    <Form.Group className={styles.form_group}>
                                        <Form.Label className={styles.label}>Full Name</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Enter your full name"
                                            onFocus={() => setActiveIcon(true)}
                                            onBlur={() => setActiveIcon(false)}
                                        />
                                        <div className={styles.iconView}>
                                            <FontAwesomeIcon icon={faUser} className={activeIcon ? styles.iconActive : styles.icon} />
                                        </div>
                                        {/* <p className='error-msg'>{'This field is required'}</p> */}
                                    </Form.Group>
                                </Col>

                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                    <Form.Group controlId="formBasicEmail" className={styles.form_group}>
                                        <Form.Label className={styles.label}>Email</Form.Label>
                                        <Form.Control
                                            type="email"
                                            placeholder="Enter your email"
                                            onFocus={() => setActiveIcon3(true)}
                                            onBlur={() => setActiveIcon3(false)}
                                        />
                                        <div className={styles.iconView}>
                                            <FontAwesomeIcon icon={faEnvelope} className={activeIcon3 ? styles.iconActive : styles.icon} />
                                        </div>
                                        {/* <p className='error-msg'>{'This field is required'}</p> */}
                                    </Form.Group>
                                </Col>
                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                    <Form.Group className={styles.form_group}>
                                        <Form.Label className={styles.label}>Address</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Enter your address"
                                            onFocus={() => setActiveIcon4(true)}
                                            onBlur={() => setActiveIcon4(false)}
                                        />
                                        <div className={styles.iconView}>
                                            <FontAwesomeIcon icon={faLocationDot} className={activeIcon4 ? styles.iconActive : styles.icon} />
                                        </div>
                                        {/* <p className='error-msg'>{'This field is required'}</p> */}
                                    </Form.Group>
                                </Col>
                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                    <Form.Group className={styles.form_group}>
                                        <Form.Label className={styles.label}>Contact Number</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Enter your contact number"
                                            onFocus={() => setActiveIcon5(true)}
                                            onBlur={() => setActiveIcon5(false)}
                                        />
                                        <div className={styles.iconView}>
                                            <FontAwesomeIcon icon={faPhone} className={activeIcon5 ? styles.iconActive : styles.icon} />
                                        </div>
                                        {/* <p className='error-msg'>{'This field is required'}</p> */}
                                    </Form.Group>
                                </Col>

                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                    <Form.Group className={styles.form_group}>
                                        <Form.Label className={styles.label}>Zip Code</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="Enter your zip code"
                                            onFocus={() => setActiveIcon9(true)}
                                            onBlur={() => setActiveIcon9(false)}
                                        />
                                        <div className={styles.iconView}>
                                            <FontAwesomeIcon icon={faLocationDot} className={activeIcon9 ? styles.iconActive : styles.icon} />
                                        </div>
                                        {/* <p className='error-msg'>{'This field is required'}</p> */}
                                    </Form.Group>
                                </Col>
                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                    <Form.Group className={styles.form_group}>
                                        <Form.Label className={styles.label}>Password</Form.Label>
                                        <Form.Control
                                            type="text"
                                            placeholder="********"
                                            onFocus={() => setActiveIcon10(true)}
                                            onBlur={() => setActiveIcon10(false)}
                                        />
                                        <div className={styles.iconView}>
                                            <FontAwesomeIcon icon={faLock} className={activeIcon10 ? styles.iconActive : styles.icon} />
                                        </div>
                                        {/* <p className='error-msg'>{'This field is required'}</p> */}
                                    </Form.Group>
                                </Col>
                                <div className={styles.viewBtn}>
                                    <Button type="submit" className={styles.btn}>Submit</Button>
                                </div>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}

export default EditCustomerComponent