"use client";
import styles from '../../../../public/scss/pages/cmsPageComponent.module.scss';
import { Button, Col, Container, Form, Row } from 'react-bootstrap'
import dynamic from 'next/dynamic';



const EditEmailTemplateComponent = () => {

    const TextEditor = dynamic(() => import('../../uiComponents/textEditorComponent'));


    return (
        <div className={styles.main}>
            <div className={styles.header}>
                <p className={styles.heading}>{'Email Template INFORMATION'}</p>
            </div>
            <div className={styles.formArea}>
                <Container fluid>
                    <Row>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Form.Group className={styles.form_group}>
                                <Form.Label className={'form-label'}>Subject<span>*</span></Form.Label>
                                <Form.Control
                                    className={styles.form_control}
                                    type="text"
                                    placeholder="Enter subject here..."
                                />
                                {/* <p className={'error-msg'}>{'This field is required'}</p> */}
                            </Form.Group>
                        </Col>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Form.Group className={styles.form_group}>
                                <Form.Label className={'form-label'}>Description<span>*</span></Form.Label>
                                <TextEditor
                                // data={values && values.description}
                                // onChange={(data) => handleChange('description', data)}
                                />
                                {/* <p className={'error-msg'}>{'This field is required'}</p> */}
                            </Form.Group>
                        </Col>
                        <div className={styles.viewBtn}>
                            <Button type="submit" className={styles.btn}>Submit</Button>
                        </div>
                    </Row>
                </Container>
            </div>
        </div>
    )
}

export default EditEmailTemplateComponent