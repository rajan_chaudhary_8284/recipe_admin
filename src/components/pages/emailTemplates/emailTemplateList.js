"use client"
import React, { useState } from 'react'
import styles from '../../../../public/scss/pages/emailTemplate.module.scss';
import { Button, Col, Dropdown, Form, Row, Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDownWideShort, faArrowUpWideShort, faCaretDown, faCaretUp, faCircle, faCircleDot, faCirclePlus, faEllipsis, faEllipsisVertical, faEye, faFilter, faMagnifyingGlass, faPen, faPenToSquare, faPencil, faPlus, faTrashCan, faXmark } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';
import { useRouter } from 'next/navigation';
import Datepicker from '@/helpers/datePicker';
import DeleteConfirmationModal from '@/components/modals/deleteConfirmationModal';

const EmailTemplateListComponent = () => {
    const [selectAll, setSelectAll] = useState(false);
    const [deletModal, setDeleteModal] = useState(false);
    const [sorting, setSorting] = useState(false);
    const route = useRouter();



    return (
        <div className={styles.main}>
            <div className={styles.buttonBar}>
                <Row>
                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                        <div className={styles.leftArea}>
                            <div className={styles.dropArea}>
                                <Dropdown
                                    className={styles.dropdown}
                                    autoClose="outside"
                                >
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>Sort by</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Title
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Subject
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                            <div className={styles.pazinationArea}>
                                <Dropdown className={styles.dropdown}>
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>10</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            10
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            20
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            30
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>

                            <Form.Group className={styles.form_group}>
                                <Form.Control
                                    type="text"
                                    placeholder="Search..."
                                    className={styles.form_control}
                                />
                                <div className={styles.iconView}>
                                    <FontAwesomeIcon icon={faMagnifyingGlass} className={styles.icon} />
                                </div>
                            </Form.Group>

                        </div>
                    </Col>
                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                        <div className={styles.rightArea}>
                            <div className={styles.dropBulkAction}>
                                <Dropdown className={styles.dropdown}>
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>Action</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faTrashCan} className={styles.dropDownIcon} />
                                            Delete
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faCircle} className={styles.dropDownIcon2} />
                                            Active
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faCircle} className={styles.dropDownIcon3} />
                                            Inactive
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                            <div className={styles.filterbtn}>
                                <Dropdown className={styles.dropdown} autoClose="outside">
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <FontAwesomeIcon icon={faFilter} className={styles.icon} />
                                        <p className={styles.selectedLabel}>Filter</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        {/* <div className={styles.cross} onClick={() => setCloseDropdown(false)} >
                                            <FontAwesomeIcon icon={faXmark} className={styles.icon} />
                                        </div> */}
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <div className={styles.created}>
                                                <div className={styles.lable}>
                                                    Created On
                                                </div>
                                                <Row >
                                                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                        <Datepicker
                                                            datePickerCustomStyle={styles.datePickerCustomStyle}
                                                        // date={startDate}
                                                        // setDate={setStartDate}
                                                        />
                                                    </Col>
                                                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                        <Datepicker
                                                            datePickerCustomStyle={styles.datePickerCustomStyle}
                                                        // date={endDate}
                                                        // setDate={setEndDAte}
                                                        />
                                                    </Col>
                                                </Row>

                                            </div>
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <div className={styles.viewBtns}>
                                            <Row>
                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Button type="submit" className={styles.cancelBtn}>
                                                        Cancel
                                                    </Button>
                                                </Col>
                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Button type="submit" className={styles.applyBtn}>
                                                        Apply
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>

            <div className={styles.listHeaders}>
                <Table responsive
                    borderless={true}
                >
                    <thead>
                        <tr style={selectAll ? { outline: `1px solid  #5149E5` } : { outline: `1px solid  transparent` }}>
                            <th style={selectAll ? { borderLeft: `1px solid  #5149E5` } : { borderLeft: `1px solid  transparent` }}>
                                <div className={styles.heading} style={{ paddingLeft: '10px' }}>
                                    <h4 >ID</h4>
                                </div>
                            </th>
                            <th>
                                <div className={styles.heading}>
                                    <h4>Title</h4>
                                </div>
                            </th>
                            <th>
                                <div className={styles.heading}>
                                    <h4>Subject</h4>
                                </div>
                            </th>
                            <th style={selectAll ? { borderRight: `1px solid  #5149E5` } : { borderRight: `1px solid  transparent` }}>
                                <div className={styles.heading}>
                                    <h4>Actions</h4>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {[0, 1, 2, 3, 4, 5, 6, 7, 8].map((item, index) => {
                            return (
                                <tr key={index} style={item.checked ? { outline: `1px solid  #5149E5` } : { outline: `1px solid  transparent` }}>
                                    <td style={item.checked ? { borderLeft: `1px solid  #5149E5` } : { borderLeft: `1px solid  transparent` }} className='align-middle'>
                                        <p className={styles.listText1} style={{ paddingLeft: '10px' }}>{1}</p>
                                    </td>

                                    <td className='align-middle'>
                                        <Link href={'/'} style={{ textDecorationLine: 'none' }}>
                                            <p className={styles.listText1}>{'Vendor Registration'}</p>
                                        </Link>
                                    </td>

                                    <td className='align-middle'>
                                        <p className={styles.listText}>{'Yous account has heen created sucesfully'}</p>
                                    </td>

                                    <td style={item.checked ? { borderRight: `1px solid  #5149E5` } : { borderRight: `1px solid  transparent` }} className='align-middle'>
                                        <div className={styles.action}>
                                            {/* <FontAwesomeIcon icon={faEye} className={styles.dropDownView} onClick={() => route.push(`/contact-us/${1}`)} /> */}
                                            <FontAwesomeIcon icon={faPenToSquare} className={styles.dropDownEdit} onClick={() => route.push(`/email-templates/edit/${1}`)} />
                                            {/* <FontAwesomeIcon icon={faTrashCan} className={styles.iconDelete} onClick={() => setDeleteModal(true)} /> */}
                                        </div>
                                    </td>
                                </tr>
                            )
                        })
                        }
                    </tbody>
                </Table>
            </div>
            {
                deletModal ?
                    <DeleteConfirmationModal
                        show={deletModal}
                        onClose={() => setDeleteModal(false)}
                        message={'Do you want to delete this Contact ?'}
                    // handleSubmit={() => handleDeleteVendor(deleteItem, deleteItemIndex)}
                    />
                    :
                    null
            }
        </div>
    )
}

export default EmailTemplateListComponent