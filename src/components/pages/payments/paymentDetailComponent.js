"use client"
import React from 'react'
import styles from '../../../../public/scss/pages/faqDetails.module.scss'


const PaymentDetailComponent = () => {


    return (
        <div className={styles.main}>
            <div className={styles.header}>
                <p className={styles.heading}>Payment Information</p>
            </div>
            <div className={styles.infoMain}>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Id :</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>1</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Full Name :</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>Rajan chaudhary</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Product/Services :</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>Black t-shit</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Category :</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>Fashion</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Transaction Id :</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>65343947</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Amount :</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>$653</p>
                    </div>
                </div>

                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Status :</p>
                    </div>
                    <div className={styles.col2}>
                        <div className={styles.tag}>
                            {/* <p className={styles.inactive}>Inactive</p> */}
                            <p className={styles.active}>Completed</p>
                        </div>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Date of Transaction :</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>Jul 12, 2023 12:10 AM</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Last modified :</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>22-12-2024 12:10 AM</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default PaymentDetailComponent