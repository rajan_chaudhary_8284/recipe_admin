"use client"
import React, { useEffect, useState } from 'react'
import styles from '../../../../public/scss/pages/vendorProductList.module.scss';
import { Button, Col, Dropdown, Form, Row, Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDownWideShort, faArrowUpWideShort, faCaretDown, faCaretUp, faCircle, faCircleDot, faCirclePlus, faEllipsis, faEllipsisVertical, faEye, faFilter, faMagnifyingGlass, faPen, faPlus, faTrashCan, faXmark } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';
import Image from 'next/image';
import product from '../../../../public/images/tshirt.jpg';
import { useRouter } from 'next/navigation';
import Datepicker from '@/helpers/datePicker';
import { Rating } from 'react-simple-star-rating';


const VendorProductListComponent = () => {

    const [selectAll, setSelectAll] = useState(false);
    const [closeDropdown, setCloseDropdown] = useState(null);
    const [activeTab, setActiveTab] = useState(0);
    const route = useRouter();

    const [list, setList] = useState([{
        id: 1,
        name: 'T-shirt',
        email: 'Fashion',
        price: 77,
        status: 1,
        checked: false,
        image: true,
        rating: 2
    },
    {
        id: 2,
        name: 'Jean',
        email: 'Fashion',
        price: 65,
        status: 0,
        checked: false,
        image: false,
        rating: 3
    },
    {
        id: 3,
        name: 'Shirt',
        email: 'Fashion',
        price: 95,
        status: 1,
        checked: false,
        image: true,
        rating: 1
    },
    {
        id: 4,
        name: 'Jacket',
        email: 'Fashion',
        price: 85,
        status: 0,
        checked: false,
        image: false,
        rating: 4
    },
    {
        id: 5,
        name: 'Cap', email: 'Fashion',
        price: 98,
        status: 1,
        checked: false,
        image: false,
        rating: 3
    },
    {
        id: 6,
        name: 'Shoes',
        email: 'Fashion',
        price: 76,
        status: 0,
        checked: false,
        image: false,
        rating: 5
    },
    {
        id: 7,
        name: 'Watch',
        email: 'Fashion',
        price: 76,
        status: 1,
        checked: false,
        image: false,
        rating: 4
    },
    {
        id: 8,
        name: 'Gloves',
        email: 'Fashion',
        price: 78,
        status: 1,
        checked: false,
        image: false,
        rating: 2
    }
    ]);


    const handleSingleCheck = (item) => {
        let array = [...list];
        let index = array.findIndex((ditem) => ditem.id == item.id);
        if (index > -1) {
            let check = array[index].checked;
            array[index].checked = !check;
        }
        setList(array);
    };

    const handleSelectAll = () => {
        let array = [...list];
        for (let i in array) {
            array[i].checked = !selectAll;
        }
        setSelectAll(!selectAll)
        setList(array);
    };

    const generateColor = () => {
        const CHHAPOLA = Math.floor(Math.random() * 16777215)
            .toString(16)
            .padStart(6, '0');
        return `#${CHHAPOLA}`;
    };

    // useEffect(() => {
    //     window.onload = () => {
    //         var colors = ['red', 'green', 'yellow', 'black'];
    //         document.querySelectorAll('.userNoImage').forEach(
    //             el => el.style.backgroundColor = colors[Math.floor(Math.random() * colors.length)]);
    //     };
    // }, [])



    return (
        <div className={styles.main}>
            <p className={styles.heading}>Vendor's product and services List</p>
            <div className={styles.tabMain}>
                <div className={activeTab == 0 ? styles.activeTab : styles.deactiveTab} onClick={() => setActiveTab(0)}>
                    <p>Products</p>
                </div>
                <div className={activeTab == 1 ? styles.activeTab : styles.deactiveTab} onClick={() => setActiveTab(1)}>
                    <p>Services</p>
                </div>
            </div>
            <div className={styles.buttonBar}>
                <Row>
                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                        <div className={styles.leftArea}>
                            <div className={styles.dropArea}>
                                <Dropdown className={styles.dropdown}>
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>Sort by</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                            Asc
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            Desc
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                            <div className={styles.pazinationArea}>
                                <Dropdown className={styles.dropdown}>
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>10</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            10
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            20
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            30
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>

                            <Form.Group className={styles.form_group}>
                                <Form.Control
                                    type="text"
                                    placeholder="Search..."
                                    className={styles.form_control}
                                />
                                <div className={styles.iconView}>
                                    <FontAwesomeIcon icon={faMagnifyingGlass} className={styles.icon} />
                                </div>
                            </Form.Group>

                        </div>
                    </Col>
                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                        <div className={styles.rightArea}>
                            <div className={styles.dropBulkAction}>
                                <Dropdown className={styles.dropdown}>
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>Action</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faTrashCan} className={styles.dropDownIcon} />
                                            Delete
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faCircle} className={styles.dropDownIcon2} />
                                            Active
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faCircle} className={styles.dropDownIcon3} />
                                            Inactive
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                            <Button type="submit" className={styles.btn} onClick={() => route.push('#')}>
                                <FontAwesomeIcon icon={faPlus} className={styles.icon} /> Add Product
                            </Button>

                            <div className={styles.filterbtn}>
                                <Dropdown className={styles.dropdown} autoClose="outside">
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <FontAwesomeIcon icon={faFilter} className={styles.icon} />
                                        <p className={styles.selectedLabel}>Filter</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        {/* <div className={styles.cross} onClick={() => setCloseDropdown(false)} >
                                            <FontAwesomeIcon icon={faXmark} className={styles.icon} />
                                        </div> */}
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <div className={styles.created}>
                                                <div className={styles.lable}>
                                                    Created On
                                                </div>
                                                <Row >
                                                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                        <Datepicker
                                                            datePickerCustomStyle={styles.datePickerCustomStyle}
                                                        // date={startDate}
                                                        // setDate={setStartDate}
                                                        />
                                                    </Col>
                                                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                        <Datepicker
                                                            datePickerCustomStyle={styles.datePickerCustomStyle}
                                                        // date={endDate}
                                                        // setDate={setEndDAte}
                                                        />
                                                    </Col>
                                                </Row>

                                            </div>
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <div className={styles.viewBtns}>
                                            <Row>
                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Button type="submit" className={styles.cancelBtn}>
                                                        Cancel
                                                    </Button>
                                                </Col>
                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Button type="submit" className={styles.applyBtn}>
                                                        Apply
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
            <div className={styles.listHeaders}>
                <Row>
                    {
                        list.map((item, index) => {
                            return (
                                <Col xxl={2} xl={2} lg={2} md={3} sm={6} xs={6} key={index}>
                                    <div className={styles.cardMain}>
                                        <div className={styles.cardImg}>
                                            <Image
                                                src={product}
                                                className={styles.image}
                                                alt='image'
                                            />
                                        </div>
                                        <div className={styles.cardBody}>
                                            <div className={styles.row}>
                                                <div className={styles.cardTitle}>{item.name}</div>
                                                <div className={styles.category}>
                                                    <p>Fashion</p>
                                                </div>
                                            </div>
                                            <div className={styles.cardRow}>
                                                <p className={styles.price}>${item.price}</p>
                                                <Rating
                                                    size={20}
                                                    fillColor={'#eeb90b'}
                                                    initialValue={item.rating}
                                                    allowHover={false}
                                                    disableFillHover={true}
                                                    readonly={true}
                                                // onClick={handleRating}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </Col>
                            )
                        })
                    }
                </Row>
            </div>
        </div>
    )
}

export default VendorProductListComponent