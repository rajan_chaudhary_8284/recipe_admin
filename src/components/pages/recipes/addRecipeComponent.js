"use client"
import styles from '../../../../public/scss/pages/addProduct.module.scss';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import UploadImageBox from '@/helpers/uploadUserImage';
import { SearchableComponent } from '@/components/uiComponents/reactSelect';
import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faXmark } from '@fortawesome/free-solid-svg-icons';
import Validation from '@/helpers/Validation';



const AddRecipeComponent = () => {

    const categoryList = [
        {
            id: 1,
            title: 'Vegetarian',
            label: 'Vegetarian',
        },
        {
            id: 2,
            title: 'Non-vegetarian',
            label: 'Non-vegetarian',
        },
    ];

    const defaultValues = {
        recipe: null,
        category: null,
        description: null,
    }

    let defaultErrors = {
        recipe: {
            rules: ['required'],
            isValid: true,
            message: '',
        },
        category: {
            rules: ['required'],
            isValid: true,
            message: '',
        },
        description: {
            rules: ['required'],
            isValid: true,
            message: '',
        }
    };


    const defaultValues1 = {
        ingredient: null,
        quantity: null,
    }


    let defaultErrors1 = {
        ingredient: {
            rules: ['required'],
            isValid: true,
            message: '',
        },
        quantity: {
            rules: ['required'],
            isValid: true,
            message: '',
        }
    };


    const [values, setValues] = useState(defaultValues);
    const [isError, setError] = useState(defaultErrors);
    const [values1, setValues1] = useState(defaultValues1);
    const [isError1, setError1] = useState(defaultErrors1);
    const [ingredientArray, setIngredientArray] = useState([defaultValues1])


    const handleChange = (field, value) => {
        let validation = new Validation(isError);
        let node = validation.validateField(field, value);
        setError({ ...isError, [field]: node });
        setValues({ ...values, [field]: value });

    };


    const handleChange1 = (field, value) => {
        let validation = new Validation(isError1);
        let node = validation.validateField(field, value);
        setError1({ ...isError1, [field]: node });
        setValues1({ ...values1, [field]: value });

    };


    const addMore = (item) => {
        const ingredientArr = [...ingredientArray];
        ingredientArr.push({ ingredient: null, quantity: null, id: 0 })
        setIngredientArray(ingredientArr)
    };


    const clearField = (item, index) => {
        const ingredientArr = [...ingredientArray];
        const indexf = ingredientArr.findIndex((itemd, indexd) => parseInt(indexd) === parseInt(index))
        if (indexf > -1) {
            ingredientArr.splice(indexf, 1)
        } else null;
        setIngredientArray(ingredientArr)
    };



    return (
        <div className={styles.main}>
            <div className={styles.formArea}>
                <div className={styles.viewImage}>
                    <UploadImageBox
                        productImg={true}
                        name={``}
                        // image={values && values.image}
                        // onResponse={(response) => {
                        //     handleChange("image", response.path)
                        // }}
                        removeImage={async () => {
                            //  await removeImage();
                        }}
                    />
                </div>
                <Container fluid>
                    <Row>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Row>
                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                    <Form.Group className={styles.form_group}>
                                        <Form.Label className={styles.label}>Recipe Name</Form.Label>
                                        <Form.Control
                                            className={styles.form_control}
                                            type="text"
                                            placeholder="Enter your recipe name"
                                            value={values && values.recipe}
                                            onChange={(e) => handleChange('recipe', e.target.value)}
                                        />
                                        {isError.recipe.message ? <p className='error-msg'>{isError.recipe.message}</p> : null}
                                    </Form.Group>
                                </Col>

                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                    <Form.Group className={styles.form_group}>
                                        <Form.Label className={styles.label}>Category</Form.Label>
                                        <SearchableComponent
                                            placeholder="Select Category"
                                            list={categoryList}
                                            value={values && values.category}
                                            onChange={(e) => {
                                                handleChange("category", e);
                                            }}
                                        />
                                        {isError.category.message ? <p className='error-msg'>{isError.category.message}</p> : null}
                                    </Form.Group>
                                </Col>

                                {ingredientArray &&
                                    ingredientArray.map((item, index) => {
                                        return (
                                            <>
                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Form.Group className={styles.form_group}>
                                                        <Form.Label className={styles.label}>Ingredients</Form.Label>
                                                        <Form.Control
                                                            className={styles.form_control}
                                                            type="text"
                                                            placeholder="Enter ingredients"
                                                            value={item && item.ingredient}
                                                            onChange={(e) => handleChange1('ingredient', e.target.value)}
                                                        />

                                                        {isError1.ingredient.message ? <p className='error-msg'>{isError1.ingredient.message}</p> : null}
                                                    </Form.Group>
                                                </Col>

                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Form.Group className={styles.form_group}>
                                                        <Form.Label className={styles.label}>Ingredients Quantity</Form.Label>
                                                        <Form.Control
                                                            className={styles.form_control}
                                                            type="text"
                                                            placeholder="Enter your Ingredients Quantity"
                                                            value={item && item.quantity}
                                                            onChange={(e) => handleChange1('quantity', e.target.value)}
                                                        />

                                                        {isError1.quantity.message ? <p className='error-msg'>{isError1.quantity.message}</p> : null}
                                                        <div className={styles.clearIcon}>
                                                            <FontAwesomeIcon icon={faXmark} style={{ fontSize: "16px" }} onClick={() => clearField(item, index)} />
                                                            {/* <p className={styles.addMore}>Remove</p> */}
                                                        </div>
                                                    </Form.Group>
                                                </Col>

                                                {index === (ingredientArray.length - 1) ?
                                                    <div className={styles.addIcon}>
                                                        <FontAwesomeIcon icon={faPlus} style={{ fontSize: "16px" }} onClick={() => addMore(item)} />
                                                        <p className={styles.addMore}>Add more</p>
                                                    </div>
                                                    : null}

                                            </>
                                        )
                                    })}


                                <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                                    <Form.Group className={styles.form_group}>
                                        <Form.Label className={styles.label}>Recipe Description</Form.Label>
                                        <Form.Control
                                            className={styles.form_control}
                                            type="text"
                                            placeholder="Enter your Recipe Description"
                                            value={values && values.description}
                                            onChange={(e) => handleChange('description', e.target.value)}
                                        />
                                        {isError.description.message ? <p className='error-msg'>{isError.description.message}</p> : null}
                                    </Form.Group>
                                </Col>

                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                    <div className={styles.checkBoxMain} >
                                        <Form.Check
                                            type="switch"
                                            id="disabled-custom-switch"
                                            label={'Do you want to publish this recipe ?'}
                                        // checked={true}
                                        // onChange={(e) => handleRemember(e.target.checked)}
                                        />
                                    </div>
                                </Col>
                                <div className={styles.viewBtn}>
                                    <Button type="submit" className={styles.btn}>Submit</Button>
                                </div>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    )
}

export default AddRecipeComponent