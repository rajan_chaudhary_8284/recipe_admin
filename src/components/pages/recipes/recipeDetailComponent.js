"use client"
import { Col, Container, Row } from 'react-bootstrap';
import { Carousel } from 'react-responsive-carousel';
import { Rating } from 'react-simple-star-rating';
import styles from '../../../../public/scss/pages/produclDetail.module.scss';
// import "react-responsive-carousel/lib/styles/carousel.min.css";
import '../../../../public/scss/pages/carousel.css';

const RecipeDetailComponent = () => {



    return (
        <div className={styles.main}>
            <Container fluid >
                <Row className={styles.rowMain}>
                    <Col xxl={4} xl={4} lg={4} md={4} sm={12} xs={12} style={{ paddingLeft: '0px', paddingRight: '0px' }}>
                        <Carousel
                            autoPlay={true}
                            interval={4000}
                            infiniteLoop={true}
                        >
                            <img
                                src={'https://media.istockphoto.com/id/1029350234/photo/pasta-with-meat-tomato-sauce-and-vegetables.jpg?s=612x612&w=0&k=20&c=clJx-V8-kh6tJUzclHnFQDRF_fMNNSYTAYno_JuXoWY='}
                                alt='image'
                            />
                            <img
                                src={'https://images.pexels.com/photos/1437267/pexels-photo-1437267.jpeg?cs=srgb&dl=pexels-enginakyurt-1437267.jpg&fm=jpg'}
                                alt='image'
                            />
                            <img
                                src={'https://thumbs.dreamstime.com/b/gourmet-tasty-italian-penne-pasta-plate-close-up-spicy-tomato-herbs-white-served-top-wooden-table-58667798.jpg'}
                                alt='image'
                            />
                        </Carousel>
                    </Col>
                    <Col xxl={8} xl={8} lg={8} md={8} sm={12} xs={12}>
                        <div className={styles.rightArea}>
                            <p className={styles.title}>
                                Bow Tie Pasta
                            </p>
                            <div className={styles.ratingRow}>
                                <Rating
                                    size={18}
                                    fillColor={'#eeb90b'}
                                    initialValue={4.5}
                                    allowHover={false}
                                    disableFillHover={true}
                                    readonly={true}
                                    allowFraction={true}
                                // onClick={handleRating}
                                />
                                <p className={styles.rating}>
                                    (4.5/5)
                                </p>
                            </div>
                            <p className={styles.veg}>Vegetarian</p>
                            {/* <p className={styles.nonVeg}>Non-vegetarian</p> */}
                            <div className={styles.activeMain}>
                                <div className={styles.dotactive} />
                                {/* <div className={styles.dotdeactive} /> */}
                                <p className={styles.active}>Active</p>
                            </div>
                            <div className={styles.line} />
                            <p className={styles.ingreTxt}>Ingredients</p>
                            <Row>
                                <Col xxl={6} xl={6} lg={6} md={6} sm={12} xs={12}>
                                    <ul>
                                        <li>1 pound penne rigate</li>
                                        <li>Coarse salt and ground pepper</li>
                                        <li>4 tablespoons olive oil</li>
                                        <li>1 medium onion, halved and thinly sliced</li>
                                        <li>4 garlic cloves, thinly sliced</li>
                                        <li>¼ teaspoon crushed red pepper</li>
                                    </ul>
                                </Col>
                                <Col xxl={6} xl={6} lg={6} md={6} sm={12} xs={12}>
                                    <ul>
                                        <li>1 large eggplant, cut into ¾-inch chunks</li>
                                        <li>1 ½ pounds plum tomatoes, cored and cut into ½-inch chunks</li>
                                        <li>2 tablespoons tomato paste</li>
                                        <li>½ cup torn fresh basil, plus more for garnish</li>
                                        <li>¾ cup ricotta cheese</li>
                                    </ul>
                                </Col>
                            </Row>
                            <div className={styles.line} />
                            <p className={styles.descriptionTitle}>Description:</p>
                            <p className={styles.description}>
                                Cook pasta in a large pot of boiling salted water until al dente, according to package instructions. Drain pasta; return to pot.Heat oil in a large skillet over medium heat. Add onion, garlic, and crushed red pepper; cook, stirring, until softened, about 5 minutes.Add eggplant to skillet; season generously with salt and pepper. Cover, and cook until eggplant begins to release juices, about 5 minutes. Uncover; cook, stirring, until tender, 3 to 4 minutes (if bottom of pan browns too much, add a few tablespoons water, and scrape with spoon).Add tomatoes, tomato paste, and 1/4 cup water to skillet; cook, stirring, until softened, about 5 minutes.Toss sauce and basil with pasta; gently reheat if necessary. Top each serving with a spoonful of ricotta, and garnish with more basil.
                            </p>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default RecipeDetailComponent