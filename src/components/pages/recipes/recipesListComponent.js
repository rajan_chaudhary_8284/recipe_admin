"use client"
import React, { useEffect, useState } from 'react'
import styles from '../../../../public/scss/pages/vendorList.module.scss';
import { Button, Col, Dropdown, Form, Row, Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDownWideShort, faArrowUpWideShort, faCaretDown, faCaretUp, faCircle, faCircleDot, faCirclePlus, faEllipsis, faEllipsisVertical, faEye, faFilter, faMagnifyingGlass, faPen, faPenToSquare, faPencil, faPlus, faTrashCan, faXmark } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';
import Image from 'next/image';
import userImg from '../../../../public/images/recipe.jpg';
import { useRouter } from 'next/navigation';
import Datepicker from '@/helpers/datePicker';
import DeleteConfirmationModal from '@/components/modals/deleteConfirmationModal';


const RecipeListComponent = () => {

    const [selectAll, setSelectAll] = useState(false);
    const [closeDropdown, setCloseDropdown] = useState(null);
    const [deletModal, setDeleteModal] = useState(false);
    const route = useRouter();

    const [list, setList] = useState([{
        id: 1,
        name: 'Chana Masala',
        category: 'Vegetarian',
        price: 77,
        status: 1,
        checked: false,
        image: true,
        rating: 2,
        stock: 11
    },
    {
        id: 2,
        name: 'Tandoori Chicken',
        category: 'Non-vegetarian',
        price: 65,
        status: 0,
        checked: false,
        image: false,
        rating: 3,
        stock: 15
    },
    {
        id: 3,
        name: 'Chicken Curry',
        category: 'Non-vegetarian',
        price: 95,
        status: 1,
        checked: false,
        image: true,
        rating: 1,
        stock: 17
    },
    {
        id: 4,
        name: 'Gajar Ka Halwa',
        category: 'Vegetarian',
        price: 85,
        status: 0,
        checked: false,
        image: false,
        rating: 4,
        stock: 7
    },
    {
        id: 5,
        name: 'Vegetarian Korma',
        category: 'Vegetarian',
        price: 98,
        status: 1,
        checked: false,
        image: false,
        rating: 3,
        stock: 19
    },
    {
        id: 6,
        name: 'Garlic Mashed',
        category: 'Vegetarian',
        price: 76,
        status: 0,
        checked: false,
        image: false,
        rating: 5,
        stock: 111
    },
    {
        id: 7,
        name: 'Sandwiches',
        category: 'Vegetarian',
        price: 76,
        status: 1,
        checked: false,
        image: false,
        rating: 4,
        stock: 119
    },
    {
        id: 8,
        name: 'Matar Paneer',
        category: 'Vegetarian',
        price: 78,
        status: 1,
        checked: false,
        image: false,
        rating: 2,
        stock: 110
    }]);

    const handleSingleCheck = (item) => {
        let array = [...list];
        let index = array.findIndex((ditem) => ditem.id == item.id);
        if (index > -1) {
            let check = array[index].checked;
            array[index].checked = !check;
        }
        setList(array);
    };


    const handleSelectAll = () => {
        let array = [...list];
        for (let i in array) {
            array[i].checked = !selectAll;
        }
        setSelectAll(!selectAll)
        setList(array);
    };


    const handleStatus = (item) => {
        let array = [...list];
        item.status = item.status == 1 ? 0 : 1;
        setList(array);
    };


    let color = [
        { color: "#fff6da", textColor: "#eeb90b" },
        { color: "#e7e7ff", textColor: "#696cff" },
        { color: "#FFD3D3", textColor: "#D81B23" },
        { color: "#E1ECCF", textColor: "#0EBE7E" },
    ];

    const [sorting, setSorting] = useState(false);

    return (
        <div className={styles.main}>
            <div className={styles.buttonBar}>
                <Row>
                    <Col xxl={6} xl={6} lg={12} md={12} sm={12} xs={12}>
                        <div className={styles.leftArea}>
                            <div className={styles.dropArea}>
                                <Dropdown
                                    className={styles.dropdown}
                                    autoClose="outside"
                                >
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>Sort by</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Name
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Recipe
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Category
                                        </Dropdown.Item>
                                        {/* <Dropdown.Divider className={styles.divider} /> */}
                                        {/* <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Price
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} /> */}
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                            <div className={styles.pazinationArea}>
                                <Dropdown className={styles.dropdown}>
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>10</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            10
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            20
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            30
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>

                            <Form.Group className={styles.form_group}>
                                <Form.Control
                                    type="text"
                                    placeholder="Search Recipe Here..."
                                    className={styles.form_control}
                                />
                                <div className={styles.iconView}>
                                    <FontAwesomeIcon icon={faMagnifyingGlass} className={styles.icon} />
                                </div>
                            </Form.Group>

                        </div>
                    </Col>
                    <Col xxl={6} xl={6} lg={12} md={12} sm={12} xs={12}>
                        <div className={styles.rightArea}>
                            <div className={styles.dropBulkAction}>
                                <Dropdown className={styles.dropdown}>
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>Action</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faTrashCan} className={styles.dropDownIcon} />
                                            Delete
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faCircle} className={styles.dropDownIcon2} />
                                            Active
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faCircle} className={styles.dropDownIcon3} />
                                            Inactive
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>

                            <Button type="submit" className={styles.btn} onClick={() => route.push('/recipes/add')}>
                                <FontAwesomeIcon icon={faPlus} className={styles.icon} /> Add Recipe
                            </Button>

                            <div className={styles.filterbtn}>
                                <Dropdown className={styles.dropdown} autoClose="outside">
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <FontAwesomeIcon icon={faFilter} className={styles.icon} />
                                        <p className={styles.selectedLabel}>Filter</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        {/* <div className={styles.cross} onClick={() => setCloseDropdown(false)} >
                                            <FontAwesomeIcon icon={faXmark} className={styles.icon} />
                                        </div> */}
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <div className={styles.created}>
                                                <div className={styles.lable}>
                                                    Created On
                                                </div>
                                                <Row >
                                                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                        <Datepicker
                                                            datePickerCustomStyle={styles.datePickerCustomStyle}
                                                        // date={startDate}
                                                        // setDate={setStartDate}
                                                        />
                                                    </Col>
                                                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                        <Datepicker
                                                            datePickerCustomStyle={styles.datePickerCustomStyle}
                                                        // date={endDate}
                                                        // setDate={setEndDAte}
                                                        />
                                                    </Col>
                                                </Row>

                                            </div>
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <div className={styles.viewBtns}>
                                            <Row>
                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Button type="submit" className={styles.cancelBtn}>
                                                        Cancel
                                                    </Button>
                                                </Col>
                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Button type="submit" className={styles.applyBtn}>
                                                        Apply
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>

            <div className={styles.listHeaders}>
                <Table responsive
                // borderless={true}
                >
                    <thead>
                        <tr style={selectAll ? { outline: `1px solid  #FF9C36` } : { outline: `1px solid  transparent` }}>
                            <th className='align-middle'>
                                <center>
                                    <Form>
                                        <Form.Check
                                            type="checkbox"
                                            id="disabled-custom-switch"
                                            checked={selectAll}
                                            onClick={() => handleSelectAll()}
                                        />
                                    </Form>
                                </center>
                            </th>
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4 >ID</h4>
                                </div>
                            </th>
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4>Photo</h4>
                                </div>
                            </th>
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4>Title</h4>
                                </div>
                            </th>
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4>Category</h4>
                                </div>
                            </th>
                            {/* <th>
                                <div className={styles.heading}>
                                    <h4>Price</h4>
                                </div>
                            </th>
                            <th>
                                <div className={styles.heading}>
                                    <h4>Stock</h4>
                                </div>
                            </th> */}
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4>Status</h4>
                                </div>
                            </th>
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4>Created</h4>
                                </div>
                            </th>
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4>Actions</h4>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        {list &&
                            list.map((item, index) => {
                                return (
                                    <tr key={index} style={item.checked ? { outline: `1px solid  #FF9C36` } : { outline: `1px solid  transparent` }}>
                                        <td className='align-middle'>
                                            <center>
                                                <Form>
                                                    <Form.Check
                                                        type="checkbox"
                                                        id="disabled-custom-switch"
                                                        checked={item.checked}
                                                        onChange={() => handleSingleCheck(item)}
                                                    />
                                                </Form>
                                            </center>
                                        </td>
                                        <td className='align-middle'>
                                            <p className={styles.listText1} >{1 + index}</p>
                                        </td>
                                        <td className='align-middle'>
                                            {item && item.image ?
                                                <div className={styles.userImage}>
                                                    <Image
                                                        src={userImg}
                                                        className={styles.image}
                                                        alt='image'
                                                    />
                                                </div>
                                                : <div style={{ backgroundColor: color[index % color.length].color }}
                                                    className={styles.userNoImage} >
                                                    <p style={{ color: color[index % color.length].textColor }}>{item && item.name ? item.name.slice(0, 1) : null}</p>
                                                </div>
                                            }
                                        </td>
                                        <td className='align-middle'>
                                            <Link href={''} style={{ textDecorationLine: 'none' }}>
                                                <p className={styles.listText1}>{item.name ? item.name : ''}</p>
                                            </Link>
                                        </td>
                                        <td className='align-middle'>
                                            <Link href={''} style={{ textDecorationLine: 'none' }}>
                                                <p className={styles.listText1}>{item.category ? item.category : ''}</p>
                                            </Link>
                                        </td>
                                        {/* <td className='align-middle'>
                                            <p className={styles.listText} >{item.price ? `$${item.price}` : ''}</p>
                                        </td> */}
                                        {/* <td className='align-middle'>
                                            <p className={styles.listText} >{item.stock ? item.stock : '9877875634'}</p>
                                        </td> */}
                                        <td className='align-middle'>
                                            {/* <div className={styles.tagMain}>
                                                <p className={item.status == 1 ? styles.successTag : item.status == 0 ? styles.cancelTag : styles.pendingTag}>
                                                    {item.status == 1 ? 'Active' : item.status == 0 ? 'Inactive' : 'Pending'}
                                                </p>
                                            </div> */}
                                            <Form.Check
                                                type="switch"
                                                id="disabled-custom-switch"
                                                label={false}
                                                checked={item.status == 1 ? true : false}
                                                onChange={() => handleStatus(item)}
                                            />
                                        </td>
                                        <td className='align-middle'>
                                            <p className={styles.listText}>{'12-12-2023 12:00'}</p>
                                        </td>
                                        <td className='align-middle'>
                                            <div className={styles.action}>
                                                <FontAwesomeIcon icon={faEye} className={styles.dropDownView} onClick={() => route.push(`/recipes/${1}`)} />
                                                <FontAwesomeIcon icon={faPenToSquare} className={styles.dropDownEdit} onClick={() => route.push(`/recipes/edit/${1}`)} />
                                                <FontAwesomeIcon icon={faTrashCan} className={styles.iconDelete} onClick={() => setDeleteModal(true)} />
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </div>
            {
                deletModal ?
                    <DeleteConfirmationModal
                        show={deletModal}
                        onClose={() => setDeleteModal(false)}
                        message={'Do you want to delete this Product ?'}
                    // handleSubmit={() => handleDeleteVendor(deleteItem, deleteItemIndex)}
                    />
                    :
                    null
            }
        </div>
    )
}

export default RecipeListComponent