"use client"
import React, { useState } from 'react'
import styles from '../../../../public/scss/pages/addProduct.module.scss';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { Rating } from 'react-simple-star-rating';



const EditReviewComponent = () => {
    const [rating, setRating] = useState(3);


    return (
        <div className={styles.main}>
            <div className={styles.formArea}>
                <Container fluid>
                    <Row>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Form.Group className={styles.form_group}>
                                <Form.Label className={styles.label}>Rating</Form.Label>
                                <div>
                                    <Rating
                                        size={20}
                                        fillColor={'#eeb90b'}
                                        initialValue={rating}
                                        allowFraction={true}
                                        onClick={(e) => setRating(e)}
                                    />
                                </div>
                                {/* <p className='error-msg'>{'This field is required'}</p> */}
                            </Form.Group>
                        </Col>
                        <Col xxl={12} xl={12} lg={12} md={12} sm={12} xs={12}>
                            <Form.Group className={styles.form_group}>
                                <Form.Label className={'form-label'}>Write Review<span>*</span></Form.Label>
                                <textarea
                                    className={styles.textArea}
                                    placeholder='Write your review...'
                                // value={values && values.answer}
                                // onInput={(e) => validate(e.target)}
                                // onChange={(e) => handleChange('answer', e.target.value)}
                                />
                                {/* <p className={'error-msg'}>{'This field is required'}</p> */}
                            </Form.Group>
                        </Col>
                        <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                            <div className={styles.checkBoxMain} >
                                <Form.Check
                                    type="switch"
                                    id="disabled-custom-switch"
                                    label={'Do you want to publish this review ?'}
                                // checked={true}
                                // onChange={(e) => handleRemember(e.target.checked)}
                                />
                            </div>
                        </Col>
                        <div className={styles.viewBtn}>
                            <Button type="submit" className={styles.btn}>Submit</Button>
                        </div>
                    </Row>
                </Container>
            </div>
        </div>
    )
}

export default EditReviewComponent