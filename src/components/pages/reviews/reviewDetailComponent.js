"use client"
import React from 'react'
import styles from '../../../../public/scss/pages/faqDetails.module.scss'
import { Rating } from 'react-simple-star-rating'


const ReviewDetailComponent = () => {


    return (
        <div className={styles.main}>
            <div className={styles.header}>
                <p className={styles.heading}>Review Information</p>
            </div>
            <div className={styles.infoMain}>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Id:</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>1</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Full Name:</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>Rajan chaudhary</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Email:</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>chaudhary721@gmail.com</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Rating:</p>
                    </div>
                    <div className={styles.col2}>
                        <div style={{ display: 'inline-flex', alignItems: 'flex-end' }} >
                            <Rating
                                size={22}
                                fillColor={'#eeb90b'}
                                initialValue={4.5}
                                allowFraction={true}
                                allowHover={false}
                                disableFillHover={true}
                                readonly={true}
                            />
                            <p className={styles.disc}>(4.5)</p>
                        </div>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Status:</p>
                    </div>
                    <div className={styles.col2}>
                        <div className={styles.tag}>
                            {/* <p className={styles.inactive}>Inactive</p> */}
                            <p className={styles.active}>Active</p>
                        </div>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Created On:</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>22-12-2024 12:10AM</p>
                    </div>
                </div>
                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Last modified:</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>22-12-2024 12:10AM</p>
                    </div>
                </div>

                <div className={styles.row}>
                    <div className={styles.col1}>
                        <p className={styles.title}>Message:</p>
                    </div>
                    <div className={styles.col2}>
                        <p className={styles.disc}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ReviewDetailComponent