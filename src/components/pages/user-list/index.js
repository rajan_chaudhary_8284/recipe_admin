"use client"
import React, { useState } from 'react'
import styles from '../../../../public/scss/pages/userList.module.scss';
import { Button, Col, Dropdown, Form, Row, Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDownWideShort, faArrowUpWideShort, faCircle, faEllipsis, faEye, faFilter, faMagnifyingGlass, faPen, faPenToSquare, faPlus, faTrashCan } from '@fortawesome/free-solid-svg-icons';
import { useRouter } from 'next/navigation';
import Datepicker from '@/helpers/datePicker';
import DeleteConfirmationModal from '@/components/modals/deleteConfirmationModal';
import userImg from '../../../../public/images/recipe.jpg';
import Image from 'next/image';

const UserListScreen = () => {
    const [selectAll, setSelectAll] = useState(false);
    const [deletModal, setDeleteModal] = useState(false);
    const route = useRouter();

    const [list, setList] = useState([{
        id: 1,
        name: 'Rajan Chaudary',
        status: 1,
        checked: false,
        image: true
    },
    {
        id: 2,
        name: 'Pritpal Virdi',
        status: 0,
        checked: false,
    },
    {
        id: 3,
        name: 'Raju Mathur',
        status: 2,
        checked: false,
    },
    {
        id: 4,
        name: 'Rajan Chaudary',
        status: 0,
        checked: false,
    },
    {
        id: 5,
        name: 'Rajan Chaudary',
        status: 1,
        checked: false,
        image: true
    },
    {
        id: 6,
        name: 'Rajan Chaudary',
        status: 2,
        checked: false,
        image: true
    },
    {
        id: 7,
        name: 'Rajan Chaudary',
        status: 1,
        checked: false,
    },
    {
        id: 8,
        name: 'Rajan Chaudary',
        status: 1,
        checked: false,
    }
    ]);


    const handleSingleCheck = (item) => {
        let array = [...list];
        let index = array.findIndex((ditem) => ditem.id == item.id);
        if (index > -1) {
            let check = array[index].checked;
            array[index].checked = !check;
        }
        setList(array);
    };


    const handleSelectAll = () => {
        let array = [...list];
        for (let i in array) {
            array[i].checked = !selectAll;
        }
        setSelectAll(!selectAll)
        setList(array);
    };

    const handleStatus = (item) => {
        let array = [...list];
        item.status = item.status == 1 ? 0 : 1;
        setList(array);
    };

    let color = [
        { color: "#fff6da", textColor: "#eeb90b" },
        { color: "#e7e7ff", textColor: "#696cff" },
        { color: "#FFD3D3", textColor: "#D81B23" },
        { color: "#E1ECCF", textColor: "#0EBE7E" },
    ];

    const [sorting, setSorting] = useState(false);

    return (
        <div className={styles.main}>
            <div className={styles.buttonBar}>
                <Row>
                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                        <div className={styles.leftArea}>
                            <div className={styles.dropArea}>
                                <Dropdown
                                    className={styles.dropdown}
                                    autoClose="outside"
                                >
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>Sort by</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Name
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Status
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Created
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                            <div className={styles.pazinationArea}>
                                <Dropdown className={styles.dropdown}>
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>10</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            10
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            20
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            30
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                            <Form.Group className={styles.form_group}>
                                <Form.Control
                                    type="text"
                                    placeholder="Search user here..."
                                    className={styles.form_control}
                                />
                                <div className={styles.iconView}>
                                    <FontAwesomeIcon icon={faMagnifyingGlass} className={styles.icon} />
                                </div>
                            </Form.Group>

                        </div>
                    </Col>
                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                        <div className={styles.rightArea}>
                            <div className={styles.dropBulkAction}>
                                <Dropdown className={styles.dropdown}>
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>Action</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faTrashCan} className={styles.dropDownIcon} />
                                            Delete
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faCircle} className={styles.dropDownIcon2} />
                                            Active
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faCircle} className={styles.dropDownIcon3} />
                                            Inactive
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                            {/* <Button type="submit" className={styles.btn} onClick={() => route.push('/')}>
                                <FontAwesomeIcon icon={faPlus} className={styles.icon} /> Add User
                            </Button> */}
                            <div className={styles.filterbtn}>
                                <Dropdown className={styles.dropdown} autoClose="outside">
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <FontAwesomeIcon icon={faFilter} className={styles.icon} />
                                        <p className={styles.selectedLabel}>Filter</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        {/* <div className={styles.cross} onClick={() => setCloseDropdown(false)} >
                                            <FontAwesomeIcon icon={faXmark} className={styles.icon} />
                                        </div> */}
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <div className={styles.created}>
                                                <div className={styles.lable}>
                                                    Created On
                                                </div>
                                                <Row >
                                                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                        <Datepicker
                                                            datePickerCustomStyle={styles.datePickerCustomStyle}
                                                        // date={startDate}
                                                        // setDate={setStartDate}
                                                        />
                                                    </Col>
                                                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                        <Datepicker
                                                            datePickerCustomStyle={styles.datePickerCustomStyle}
                                                        // date={endDate}
                                                        // setDate={setEndDAte}
                                                        />
                                                    </Col>
                                                </Row>

                                            </div>
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <div className={styles.viewBtns}>
                                            <Row>
                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Button type="submit" className={styles.cancelBtn}>
                                                        Cancel
                                                    </Button>
                                                </Col>
                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Button type="submit" className={styles.applyBtn}>
                                                        Apply
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>
            <div className={styles.listHeaders}>
                
                <Table responsive
                    borderless={true}
                >
                    <thead>
                        <tr style={selectAll ? { outline: `1px solid  #FF9C36` } : { outline: `1px solid  transparent` }}>
                            <th className='align-middle'>
                                <center>
                                    <Form>
                                        <Form.Check
                                            type="checkbox"
                                            id="disabled-custom-switch"
                                            checked={selectAll}
                                            onClick={() => handleSelectAll()}
                                        />
                                    </Form>
                                </center>
                            </th>
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4 >ID</h4>
                                </div>
                            </th>
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4>Name</h4>
                                </div>
                            </th>
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4>Status</h4>
                                </div>
                            </th>
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4>Created</h4>
                                </div>
                            </th>
                            <th className='align-middle'>
                                <div className={styles.heading}>
                                    <h4>Actions</h4>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {list &&
                            list.map((item, index) => {
                                return (
                                    <tr key={index} style={item.checked ? { outline: `1px solid  #FF9C36` } : { outline: `1px solid  transparent` }}>
                                        <td className='align-middle'>
                                            <center>
                                                <Form>
                                                    <Form.Check
                                                        type="checkbox"
                                                        id="disabled-custom-switch"
                                                        checked={item.checked}
                                                        onChange={() => handleSingleCheck(item)}
                                                    />
                                                </Form>
                                            </center>
                                        </td>
                                        <td className='align-middle'>
                                            <p className={styles.listText1} >{index + 1}</p>
                                        </td>
                                        <td className='align-middle'>
                                            <div className={styles.nameMain}>
                                                {
                                                    item && item.image ?
                                                        <div className={styles.userImage}>
                                                            <Image
                                                                src={userImg}
                                                                className={styles.image}
                                                                height={200}
                                                                width={200}
                                                                alt='image'
                                                            />
                                                        </div>
                                                        :
                                                        <div style={{ backgroundColor: color[index % color.length].color }}
                                                            className={styles.userNoImage} >
                                                            <p style={{ color: color[index % color.length].textColor }}>{item && item.name ? item.name.slice(0, 1) : null}</p>
                                                        </div>
                                                }
                                                <p className={styles.listText1}>{item.name ? item.name : '-'}</p>
                                            </div>
                                        </td>
                                        {/* <td className='align-middle'>
                                        </td> */}
                                        <td className='align-middle'>
                                            <Form.Check
                                                type="switch"
                                                id="disabled-custom-switch"
                                                label={false}
                                                checked={item.status == 1 ? true : false}
                                                onChange={() => handleStatus(item)}
                                            />
                                            {/* <div className={styles.tagMain}>
                                                <p className={item.status == 0 ? styles.cancelTag : styles.successTag}>
                                                    {item.status == 0 ? 'Inactive' : 'Active'}
                                                </p>
                                            </div> */}
                                        </td>
                                        <td>
                                            <p className={styles.listText}>{'12-12-2023 12:00'}</p>
                                        </td>
                                        <td className='align-middle'>
                                            <div className={styles.action}>
                                                <FontAwesomeIcon icon={faEye} className={styles.dropDownView} onClick={() => route.push(`/vendors/${1}`)} />
                                                <FontAwesomeIcon icon={faPenToSquare} className={styles.dropDownEdit} onClick={() => route.push(`/vendors/edit/${1}`)} />
                                                <FontAwesomeIcon icon={faTrashCan} className={styles.iconDelete} onClick={() => setDeleteModal(true)} />
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </div>
            {
                deletModal ?
                    <DeleteConfirmationModal
                        show={deletModal}
                        onClose={() => setDeleteModal(false)}
                        message={'Do you want to delete this user ?'}
                    // handleSubmit={() => handleDeleteVendor(deleteItem, deleteItemIndex)}
                    />
                    :
                    null
            }
        </div>
    )
}

export default UserListScreen