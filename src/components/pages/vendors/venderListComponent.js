"use client"
import React, { useEffect, useState } from 'react'
import styles from '../../../../public/scss/pages/vendorList.module.scss';
import { Button, Col, Dropdown, Form, Row, Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDownWideShort, faArrowUpWideShort, faCaretDown, faCaretUp, faCircle, faCircleDot, faCirclePlus, faEllipsis, faEllipsisVertical, faEye, faFilter, faMagnifyingGlass, faPen, faPenToSquare, faPencil, faPlus, faTrashCan, faXmark } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';
import Image from 'next/image';
import userImg from '../../../../public/images/user.png';
import { useRouter } from 'next/navigation';
import Datepicker from '@/helpers/datePicker';
import DeleteConfirmationModal from '@/components/modals/deleteConfirmationModal';

const VenderListComponent = () => {
    const [selectAll, setSelectAll] = useState(false);
    const [closeDropdown, setCloseDropdown] = useState(null);
    const [deletModal, setDeleteModal] = useState(false);
    const route = useRouter();

    const [list, setList] = useState([{
        id: 1,
        name: 'Rajan',
        bussinessName: 'Rajan chaudhary',
        email: 'rajan@gmail.com',
        phoneNumber: 9878656765,
        status: 1,
        checked: false,
        image: true,
    },
    {
        id: 2,
        name: 'Raman',
        bussinessName: 'Raman singh',
        email: 'raman@gmail.com',
        phoneNumber: 9878656765,
        status: 0,
        checked: false,
        image: false,
    },
    {
        id: 3,
        name: 'Harman',
        bussinessName: 'Harmanpreet singh',
        email: 'harman@gmail.com',
        phoneNumber: 9878656765,
        status: 1,
        checked: false,
        image: false,
    },
    {
        id: 4,
        name: 'Ravi',
        bussinessName: 'Ravi kumar',
        email: 'ravi@gmail.com',
        phoneNumber: 9878656765,
        status: 0,
        checked: false,
        image: true,
    },
    {
        id: 5,
        name: 'Pritpal',
        bussinessName: 'Pritpal singh',
        email: 'pritpal@gmail.com',
        phoneNumber: 9878656765,
        status: 1,
        checked: false,
        image: false,
    },
    {
        id: 6,
        name: 'Raju',
        bussinessName: 'Raju mathur',
        email: 'raju@gmail.com',
        phoneNumber: 9878656765,
        status: 1,
        checked: false,
        image: false,
    },
    {
        id: 7,
        name: 'Vivek',
        bussinessName: 'Vivek Kumar',
        email: 'vivek@gmail.com',
        phoneNumber: 7878656765,
        status: 1,
        checked: false,
        image: true,
    },
    {
        id: 8,
        name: 'Karan',
        bussinessName: 'Karan Kumar',
        email: 'karan@gmail.com',
        phoneNumber: 8878656765,
        status: 1,
        checked: false,
        image: false,
    }
    ]);

    const handleSingleCheck = (item) => {
        let array = [...list];
        let index = array.findIndex((ditem) => ditem.id == item.id);
        if (index > -1) {
            let check = array[index].checked;
            array[index].checked = !check;
        }
        setList(array);
    };


    const handleSelectAll = () => {
        let array = [...list];
        for (let i in array) {
            array[i].checked = !selectAll;
        }
        setSelectAll(!selectAll)
        setList(array);
    };

    const handleStatus = (item) => {
        let array = [...list];
        item.status = item.status == 1 ? 0 : 1;
        setList(array);
    }


    let color = [
        { color: "#fff6da", textColor: "#eeb90b" },
        { color: "#e7e7ff", textColor: "#696cff" },
        { color: "#FFD3D3", textColor: "#D81B23" },
        { color: "#E1ECCF", textColor: "#0EBE7E" },
    ];

    const [sorting, setSorting] = useState(false);

    return (
        <div className={styles.main}>
            <div className={styles.buttonBar}>
                <Row>
                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                        <div className={styles.leftArea}>
                            <div className={styles.dropArea}>
                                <Dropdown
                                    className={styles.dropdown}
                                    autoClose="outside"
                                >
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>Sort by</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Name
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Email
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Contact Number
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Status
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item} onClick={() => setSorting(!sorting)}>
                                            {
                                                sorting ?
                                                    <FontAwesomeIcon icon={faArrowUpWideShort} className={styles.dropDownIcon} />
                                                    :
                                                    <FontAwesomeIcon icon={faArrowDownWideShort} className={styles.dropDownIcon} />
                                            }
                                            Created
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                            <div className={styles.pazinationArea}>
                                <Dropdown className={styles.dropdown}>
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>10</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            10
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            20
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            30
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>

                            <Form.Group className={styles.form_group}>
                                <Form.Control
                                    type="text"
                                    placeholder="Search..."
                                    className={styles.form_control}
                                />
                                <div className={styles.iconView}>
                                    <FontAwesomeIcon icon={faMagnifyingGlass} className={styles.icon} />
                                </div>
                            </Form.Group>

                        </div>
                    </Col>
                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                        <div className={styles.rightArea}>
                            <div className={styles.dropBulkAction}>
                                <Dropdown className={styles.dropdown}>
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <p className={styles.selectedLabel}>Action</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faTrashCan} className={styles.dropDownIcon} />
                                            Delete
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faCircle} className={styles.dropDownIcon2} />
                                            Active
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <FontAwesomeIcon icon={faCircle} className={styles.dropDownIcon3} />
                                            Inactive
                                        </Dropdown.Item>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                            <Button type="submit" className={styles.btn} onClick={() => route.push('/vendors/add')}>
                                <FontAwesomeIcon icon={faPlus} className={styles.icon} /> Add Vendor
                            </Button>

                            <div className={styles.filterbtn}>
                                <Dropdown className={styles.dropdown} autoClose="outside">
                                    <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                        <FontAwesomeIcon icon={faFilter} className={styles.icon} />
                                        <p className={styles.selectedLabel}>Filter</p>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_menu}>
                                        {/* <div className={styles.cross} onClick={() => setCloseDropdown(false)} >
                                            <FontAwesomeIcon icon={faXmark} className={styles.icon} />
                                        </div> */}
                                        <Dropdown.Item className={styles.dropdown_item}>
                                            <div className={styles.created}>
                                                <div className={styles.lable}>
                                                    Created On
                                                </div>
                                                <Row >
                                                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                        <Datepicker
                                                            datePickerCustomStyle={styles.datePickerCustomStyle}
                                                        // date={startDate}
                                                        // setDate={setStartDate}
                                                        />
                                                    </Col>
                                                    <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                        <Datepicker
                                                            datePickerCustomStyle={styles.datePickerCustomStyle}
                                                        // date={endDate}
                                                        // setDate={setEndDAte}
                                                        />
                                                    </Col>
                                                </Row>

                                            </div>
                                        </Dropdown.Item>
                                        <Dropdown.Divider className={styles.divider} />
                                        <div className={styles.viewBtns}>
                                            <Row>
                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Button type="submit" className={styles.cancelBtn}>
                                                        Cancel
                                                    </Button>
                                                </Col>
                                                <Col xxl={6} xl={6} lg={6} md={6} sm={6} xs={6}>
                                                    <Button type="submit" className={styles.applyBtn}>
                                                        Apply
                                                    </Button>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Dropdown.Menu>
                                </Dropdown>
                            </div>
                        </div>
                    </Col>
                </Row>
            </div>

            <div className={styles.listHeaders}>
                <Table responsive
                    borderless={true}
                >
                    <thead>
                        <tr style={selectAll ? { outline: `1px solid  #5149E5` } : { outline: `1px solid  transparent` }}>
                            <th style={selectAll ? { borderLeft: `1px solid  #5149E5` } : { borderLeft: `1px solid  transparent` }}>
                                <center>
                                    <Form>
                                        <Form.Check
                                            type="checkbox"
                                            id="disabled-custom-switch"
                                            checked={selectAll}
                                            onClick={() => handleSelectAll()}
                                        />
                                    </Form>
                                </center>
                            </th>
                            <th>
                                <div className={styles.heading}>
                                    <h4 >ID</h4>
                                    {/* <div className={styles.sortingArrows} >
                                        <FontAwesomeIcon
                                            icon={faCaretUp}
                                            className={styles.arrowup}
                                        />
                                        <FontAwesomeIcon
                                            icon={faCaretDown}
                                            className={styles.arrowdown}
                                        />
                                    </div> */}
                                </div>
                            </th>
                            <th>
                                <div className={styles.heading}>
                                    <h4>Photo</h4>
                                    {/* <div className={styles.sortingArrows} >
                                        <FontAwesomeIcon
                                            icon={faCaretUp}
                                            className={styles.arrowup}
                                        />
                                        <FontAwesomeIcon
                                            icon={faCaretDown}
                                            className={styles.arrowdown}
                                        />
                                    </div> */}
                                </div>
                            </th>
                            <th>
                                <div className={styles.heading}>
                                    <h4>Name</h4>
                                    {/* <div className={styles.sortingArrows} >
                                        <FontAwesomeIcon
                                            icon={faCaretUp}
                                            className={styles.arrowup}
                                        />
                                        <FontAwesomeIcon
                                            icon={faCaretDown}
                                            className={styles.arrowdown}
                                        />
                                    </div> */}
                                </div>
                            </th>
                            <th>
                                <div className={styles.heading}>
                                    <h4>Business Name</h4>
                                    {/* <div className={styles.sortingArrows} >
                                        <FontAwesomeIcon
                                            icon={faCaretUp}
                                            className={styles.arrowup}
                                        />
                                        <FontAwesomeIcon
                                            icon={faCaretDown}
                                            className={styles.arrowdown}
                                        />
                                    </div> */}
                                </div>
                            </th>
                            <th>
                                <div className={styles.heading}>
                                    <h4>Email</h4>
                                </div>
                            </th>
                            <th>
                                <div className={styles.heading}>
                                    <h4>Contact Number</h4>
                                </div>
                            </th>
                            <th>
                                <div className={styles.heading}>
                                    <h4>Status</h4>
                                </div>
                            </th>
                            <th>
                                <div className={styles.heading}>
                                    <h4>Created</h4>
                                </div>
                            </th>
                            <th style={selectAll ? { borderRight: `1px solid  #5149E5` } : { borderRight: `1px solid  transparent` }}>
                                <div className={styles.heading}>
                                    <h4>Actions</h4>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        {list &&
                            list.map((item, index) => {
                                return (
                                    <tr key={index} style={item.checked ? { outline: `1px solid  #5149E5` } : { outline: `1px solid  transparent` }}>
                                        <td style={item.checked ? { borderLeft: `1px solid  #5149E5` } : { borderLeft: `1px solid  transparent` }} className='align-middle'>
                                            <center>
                                                <Form>
                                                    <Form.Check
                                                        type="checkbox"
                                                        id="disabled-custom-switch"
                                                        checked={item.checked}
                                                        onChange={() => handleSingleCheck(item)}
                                                    />
                                                </Form>
                                            </center>
                                        </td>
                                        <td className='align-middle'>
                                            <p className={styles.listText1} >{1}</p>
                                        </td>
                                        <td className='align-middle'>
                                            {item && item.image ?
                                                <div className={styles.userImage}>
                                                    <Image
                                                        src={userImg}
                                                        className={styles.image}
                                                        alt='image'
                                                    />
                                                </div>
                                                : <div style={{ backgroundColor: color[index % color.length].color }}
                                                    className={styles.userNoImage} >
                                                    <p style={{ color: color[index % color.length].textColor }}>{item && item.name ? item.name.slice(0, 1) : null}</p>
                                                </div>
                                            }
                                        </td>
                                        <td className='align-middle'>
                                            <Link href={'/'} style={{ textDecorationLine: 'none' }}>
                                                <p className={styles.listText1}>{item.name ? item.name : 'Rajan'}</p>
                                            </Link>
                                        </td>
                                        <td className='align-middle'>
                                            <Link href={'/'} style={{ textDecorationLine: 'none' }}>
                                                <p className={styles.listText1}>{item.bussinessName ? item.bussinessName : 'Rajan chaudhary'}</p>
                                            </Link>
                                        </td>
                                        <td className='align-middle'>
                                            <p className={styles.listText} >{item.email ? item.email : 'rajan.c@globiztechnology.com'}</p>
                                        </td>
                                        <td className='align-middle'>
                                            <p className={styles.listText} >{item.phoneNumber ? item.phoneNumber : '9877875634'}</p>
                                        </td>
                                        <td className='align-middle'>
                                            <Form.Check
                                                type="switch"
                                                id="disabled-custom-switch"
                                                label={false}
                                                checked={item.status == 1 ? true : false}
                                                onChange={() => handleStatus(item)}
                                            />
                                            {/* <div className={styles.tagMain}>
                                                <p className={item.status == 1 ? styles.successTag : item.status == 0 ? styles.cancelTag : styles.pendingTag}>
                                                    {item.status == 1 ? 'Complete' : item.status == 0 ? 'Canceled' : 'Pending'}
                                                </p>
                                            </div> */}
                                        </td>
                                        <td className='align-middle'>
                                            <p className={styles.listText}>{'12-12-2023 12:00'}</p>
                                        </td>
                                        <td style={item.checked ? { borderRight: `1px solid  #5149E5` } : { borderRight: `1px solid  transparent` }} className='align-middle'>
                                            {/* <Dropdown className={styles.dropdown}>
                                                <Dropdown.Toggle variant="none" id="dropdown-basic" className={styles.dropdown_toggle}>
                                                    <FontAwesomeIcon icon={faEllipsis}
                                                        className={styles.icon}
                                                    />
                                                </Dropdown.Toggle>
                                                <Dropdown.Menu className={styles.dropdown_menu}>
                                                    <Dropdown.Item className={styles.dropdown_item} onClick={() => route.push(`/vendors/${1}`)}>
                                                        <FontAwesomeIcon icon={faEye} className={styles.dropDownView} />
                                                        View
                                                    </Dropdown.Item>
                                                    <Dropdown.Divider className={styles.divider} />
                                                    <Dropdown.Item className={styles.dropdown_item} onClick={() => route.push(`/vendors/edit/${1}`)} >
                                                        <FontAwesomeIcon icon={faPen} className={styles.dropDownEdit} />
                                                        Edit
                                                    </Dropdown.Item>
                                                    <Dropdown.Divider className={styles.divider} />
                                                    <Dropdown.Item className={styles.dropdown_item} onClick={() => setDeleteModal(true)} >
                                                        <FontAwesomeIcon icon={faTrashCan} className={styles.iconDelete} />
                                                        Delete
                                                    </Dropdown.Item>
                                                </Dropdown.Menu>
                                            </Dropdown> */}
                                            <div className={styles.action}>
                                                <FontAwesomeIcon icon={faEye} className={styles.dropDownView} onClick={() => route.push(`/vendors/${1}`)} />
                                                <FontAwesomeIcon icon={faPenToSquare} className={styles.dropDownEdit} onClick={() => route.push(`/vendors/edit/${1}`)} />
                                                <FontAwesomeIcon icon={faTrashCan} className={styles.iconDelete} onClick={() => setDeleteModal(true)} />
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </Table>
            </div>
            {
                deletModal ?
                    <DeleteConfirmationModal
                        show={deletModal}
                        onClose={() => setDeleteModal(false)}
                        message={'Do you want to delete this Vendor ?'}
                    // handleSubmit={() => handleDeleteVendor(deleteItem, deleteItemIndex)}
                    />
                    :
                    null
            }
        </div>
    )
}

export default VenderListComponent