import React from 'react'
import styles from '../../../../public/scss/pages/vendorDetail.module.scss';
import { Col, Row } from 'react-bootstrap';
import Image from 'next/image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock, faEnvelope, faLocation, faPhone, faUser, faUserCheck, faUserTie } from '@fortawesome/free-solid-svg-icons';
import userImg from '../../../../public/images/userImg1.jpg';

const VendorDetailsComponent = () => {


    return (
        <div className={styles.main}>
            <Row className={styles.rowMain}>
                <Col xxl={4} xl={4} lg={4} md={4} sm={4} xs={4}>
                    <div className={styles.profileContainer} >
                        <div className={styles.profileMain}>
                            <Image
                                src={userImg}
                                className={styles.image}
                                alt='image'
                            />
                        </div>
                        <div className={styles.tag}>
                            <p>Vendor</p>
                        </div>
                        <p className={styles.title}>Rajan Chaudhary</p>
                    </div>
                </Col>
                <Col xxl={8} xl={8} lg={8} md={8} sm={8} xs={6}>
                    <div className={styles.profileInfoBox}>
                        <div className={styles.row}>
                            <div className={styles.left}>
                                <FontAwesomeIcon icon={faUser} className={styles.icon} />
                                <p className={styles.titleTxt}>Name</p>
                            </div>
                            <div className={styles.right}>
                                <p className={styles.infoTxt}>Rajan Chaudhary</p>
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.left}>
                                <FontAwesomeIcon icon={faUserTie} className={styles.icon} />
                                <p className={styles.titleTxt}>Business Name</p>
                            </div>
                            <div className={styles.right}>
                                <p className={styles.infoTxt}>Rajan Chaudhary</p>
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.left}>
                                <FontAwesomeIcon icon={faEnvelope} className={styles.icon} />
                                <p className={styles.titleTxt}>Email</p>
                            </div>
                            <div className={styles.right}>
                                <p className={styles.infoTxt}>rajan721@gmail.com</p>
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.left}>
                                <FontAwesomeIcon icon={faPhone} className={styles.icon} />
                                <p className={styles.titleTxt}>Phone number</p>
                            </div>
                            <div className={styles.right}>
                                <p className={styles.infoTxt}>08848782548</p>
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.left}>
                                <FontAwesomeIcon icon={faLocation} className={styles.icon} />
                                <p className={styles.titleTxt}>Address</p>
                            </div>
                            <div className={styles.right}>
                                <p className={styles.infoTxt}>Doraha, Ludhiana</p>
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.left}>
                                <FontAwesomeIcon icon={faUserCheck} className={styles.icon} />
                                <p className={styles.titleTxt}>Status</p>
                            </div>
                            <div className={styles.right}>
                                <div className={styles.tag} >
                                    <p className={styles.activeTag}>Active</p>
                                    {/* <p className={styles.deactiveTag}>Inactive</p> */}
                                </div>
                            </div>
                        </div>
                        <div className={styles.row}>
                            <div className={styles.left}>
                                <FontAwesomeIcon icon={faClock} className={styles.icon} />
                                <p className={styles.titleTxt}>Created</p>
                            </div>
                            <div className={styles.right}>
                                <p className={styles.infoTxt}>12-08-2024 12:00</p>
                            </div>
                        </div>
                    </div>
                </Col>
            </Row>
        </div>
    )
}

export default VendorDetailsComponent