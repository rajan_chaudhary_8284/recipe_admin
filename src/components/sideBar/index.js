'use client'
import styles from '../../../public/scss/pages/sidebar.module.scss'
import logo from '../../../public/images/logo.png'
import Image from 'next/image'
import { useEffect, useState } from 'react';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Accordion, Nav, Navbar } from 'react-bootstrap';
import {
    faAngleRight, faBowlRice, faBriefcase, faBuilding, faBusinessTime, faCartShopping, faClock, faEnvelope, faExclamationCircle, faFileCircleCheck, faFileLines, faGear, faHandHoldingDollar, faHouse,
    faLayerGroup,
    faLocationDot,
    faMapLocation,
    faMessage, faNewspaper, faPeopleGroup, faPhone, faRectangleList, faSackDollar, faShieldHalved, faStar, faStore, faUserGroup, faUserTie, faUsers,
    faUtensils
} from '@fortawesome/free-solid-svg-icons';
import { usePathname } from 'next/navigation';
import '../../../public/scss/pages/accordion.css';

const Sidebar = () => {
    const currentPath = usePathname();
    const [list, setList] = useState([
        // {
        //     name: 'Dashboard',
        //     action: '/dashboard',
        //     sub_List: false,
        //     iconName: faHouse,
        //     id: 1,
        //     checked: false
        // },
        // {
        //     name: 'Vendors',
        //     action: '/vendors',
        //     sub_List: false,
        //     iconName: faUsers,
        //     id: 2,
        //     checked: false
        // },
        // {
        //     name: 'Customers',
        //     action: '/customers',
        //     sub_List: false,
        //     iconName: faPeopleGroup,
        //     id: 11,
        //     checked: false
        // },
        {
            name: 'Recipe',
            action: '/recipes',
            id: 9,
            iconName: faUtensils,
        },
        {
            name: 'Users',
            action: '/user-list',
            id: 5,
            iconName: faUserGroup,
        },
        // {
        //     name: 'Orders',
        //     action: '/orders',
        //     id: 12,
        //     iconName: faCartShopping,
        // },
        // {
        //     name: 'Payments',
        //     action: '/payments',
        //     iconName: faSackDollar,
        //     id: 8,
        //     checked: false
        // },
        // {
        //     name: 'Reviews',
        //     action: '/reviews',
        //     iconName: faStar,
        //     id: 8,
        //     checked: false
        // },
        // {
        //     name: 'Contact Request',
        //     action: '/contact-us',
        //     sub_List: false,
        //     iconName: faPhone,
        //     id: 4,
        //     checked: false
        // },
        // {
        //     name: 'Email Templates',
        //     action: '/email-templates',
        //     iconName: faEnvelope,
        //     id: 5,
        //     checked: false
        // },
        {
            name: 'CMS pages',
            action: '',
            id: 3,
            sub_List: true,
            iconName: faLayerGroup,
            submenu: [
                // {
                //     name: 'Intro Screen',
                //     action: '/cms/intro',
                //     iconName: faRectangleList,
                //     id: 1,
                //     checked: false
                // },
                {
                    name: 'About Us',
                    action: '/cms/about-us',
                    iconName: faExclamationCircle,
                    id: 2,
                    checked: false
                },
                {
                    name: 'Privacy Policy',
                    action: '/cms/privacy-policy',
                    iconName: faShieldHalved,
                    id: 3,
                    checked: false
                },
                {
                    name: 'Terms & Conditions',
                    action: '/cms/terms-conditions',
                    iconName: faFileCircleCheck,
                    id: 4,
                    checked: false
                },
                {
                    name: 'FAQ',
                    action: '/cms/faq',
                    iconName: faMessage,
                    id: 5,
                    checked: false
                }
            ]
        },
        // {
        //     name: 'Settings',
        //     action: '/settings',
        //     iconName: faGear,
        //     id: 6,
        //     checked: false
        // },

    ]);
    const [expanded, setExpanded] = useState(false);
    const [activeAccordion, setActiveAccordion] = useState(null);
    const [scrollActive, setScrollActive] = useState(false);


    const handleAccordionClick = async (itemId) => {
        await localStorage.setItem('ACCORDI0N_ID', JSON.stringify(itemId));
        if (activeAccordion === itemId) {
            setActiveAccordion(null);
        } else {
            setActiveAccordion(itemId);
        }
    };


    useEffect(() => {
        let item = localStorage.getItem('ACCORDI0N_ID');
        item = JSON.parse(item);
        let data = item;
        if (currentPath == '/cms/intro' || currentPath == '/cms/about-us' || currentPath == '/cms/privacy-policy' ||
            currentPath == '/cms/terms-conditions' || currentPath == '/cms/faq') {
            if (data) {
                setActiveAccordion(data)
            } else {
                setActiveAccordion(null)
            }
        } else {
            localStorage.setItem('ACCORDI0N_ID', null);
        }
    }, [currentPath]);


    useEffect(() => {
        if (!scrollActive) {
            document.body.style.overflow = 'auto';
            document.body.style.position = 'relative';
        } else {
            document.body.style.overflow = 'hidden';
            document.body.style.position = 'fixed';
            document.body.style.width = '100%';
        }
    }, [scrollActive]);


    return (
        <div className={styles.main}>
            <Navbar expand="lg" className={styles.navbar}>
                <Navbar.Brand className={styles.navbarHeader}>
                    <div className={styles.viewImg}>
                        <Image
                            src={logo}
                            className={styles.image}
                            alt='logo'
                            priority={true}
                        />
                    </div>
                    <p className={styles.title}>Recipe Hunter</p>
                </Navbar.Brand>
                <Navbar.Toggle className={styles.toggleBtn}
                    onClick={() => (setExpanded(!expanded), setScrollActive(!scrollActive))}
                />
                <Navbar.Collapse>
                    <Nav className={styles.navItem}>
                        <div className={styles.menuMain}>
                            <ul>
                                {list.map((item, index) => {
                                    return (
                                        <div key={index}>
                                            <li
                                                className={item.sub_List ? styles.listInactive : currentPath == item.action ? styles.listActive : styles.listInactive}
                                            >
                                                {
                                                    item && item.sub_List ?
                                                        <div style={{ width: '100%' }}>
                                                            <Accordion activeKey={activeAccordion}
                                                                onSelect={(eventKey) => handleAccordionClick(eventKey)}>
                                                                <Accordion.Item eventKey={item.id}>
                                                                    <Accordion.Header>
                                                                        <div className='left'>
                                                                            <FontAwesomeIcon
                                                                                icon={item.iconName}
                                                                            />
                                                                        </div>
                                                                        <div className='center'>
                                                                            {item.name}
                                                                        </div>
                                                                        <div className='right'>
                                                                            <FontAwesomeIcon
                                                                                icon={faAngleRight}
                                                                                className='class-icon'
                                                                            />
                                                                        </div>
                                                                    </Accordion.Header>
                                                                    <Accordion.Body>
                                                                        <ul className='main-body'>
                                                                            {
                                                                                item.submenu.map((data, index) => (
                                                                                    <li className={currentPath === data.action ? 'text-body active' : 'text-body'} key={index} onClick={() => (setExpanded(false), setScrollActive(false))}>
                                                                                        <Link href={data.action}>
                                                                                            <FontAwesomeIcon
                                                                                                icon={data.iconName}
                                                                                                className='icon-left'
                                                                                            />

                                                                                            {data.name}
                                                                                        </Link>
                                                                                    </li>
                                                                                ))
                                                                            }
                                                                        </ul>
                                                                    </Accordion.Body>
                                                                </Accordion.Item>
                                                            </Accordion>
                                                        </div>
                                                        :
                                                        <div className={styles.row} onClick={() => (setExpanded(false), setScrollActive(false), handleAccordionClick(null))}>
                                                            <Link href={item.action} >
                                                                <div className={styles.left}>
                                                                    <div className={styles.viewIcon}>
                                                                        <FontAwesomeIcon
                                                                            icon={item.iconName}
                                                                            className={styles.icon}
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div className={styles.right}>
                                                                    <p className={styles.itemtxt}>
                                                                        {item.name}
                                                                    </p>
                                                                </div>
                                                            </Link>
                                                        </div>
                                                }
                                            </li>
                                        </div>
                                    )
                                })}
                            </ul>
                        </div>
                    </Nav>
                </Navbar.Collapse>
            </Navbar >
        </div >
    )
};


export default Sidebar