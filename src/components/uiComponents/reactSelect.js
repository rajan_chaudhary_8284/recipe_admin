import { useState } from 'react';
import Select, { components } from 'react-select';
import styles from '../../../public/scss/pages/reactSelet.module.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';


export const CustomSelectOptions = (props) => {
    const { Option } = components

    return (
        <Option {...props.items} >
            <div style={{
                display: 'flex', alignItems: 'center', justifyContent: 'space-between',
                marginInline: '-12px',
                paddingInline: '12px',
                backgroundColor: props && props.value && props.value.label === props.items.data.label ? '#ffebd9' : null
            }}>
                {props && props.items && props.items.data && props.items.data.label ? props.items.data.label : ''}
                {
                    props && props.value && props.value.label === props.items.data.label ?
                        <FontAwesomeIcon
                            style={{ float: 'right', color: '#FF7A00' }}
                            icon={faCheck}
                        />
                        :
                        null
                }
            </div>
        </Option>
    )
};


export const SearchableComponent = (props) => {
    const { list, placeholder, value, minHeight38, marginBottom, isDisabled } = props

    const customStyles = {
        container: (provided) => ({
            ...provided,
            flex: 1,
            backgroundColor: 'white',
        }),
        control: (provided, state) => ({
            ...provided,
            // width: minHeight38 ? '332px' : "auto",
            height: minHeight38 ? '40px' : '50px',
            borderColor: 'rgb(201, 201, 200);',
            borderRadius: '4px',
            fontSize: '14px',
            color: '#E8E7E7',
            paddingLeft: '2px',
            backgroundColor: 'white', // Set the background color of the control to white
            marginBottom: marginBottom ? '0px' : '15px'
        }),
        // singleValue: (provided, state) => {
        //     const opacity = state.isDisabled ? 0.5 : 1;
        //     const transition = 'opacity 300ms';

        //     return { ...provided, opacity, transition };
        // },
        option: (provided, state) => ({
            ...provided,
            backgroundColor: 'white', // Set the background color of individual options to white
            color: 'black', // Set the text color of individual options to black
            cursor: 'pointer'
        }),
        menu: (provided, state) => ({
            ...provided,
            zIndex: 9999, // Ensure the dropdown appears above other elements
        }),
        placeholder: (provided) => ({
            ...provided,
            fontSize: '14px',
            color: '#c5c5c5',
            paddingLeft: '1px',
        }),
        valueContainer: (provided, state) => ({
            ...provided,
            //----------Where the input value is ---------------------
        }),
        menuList: base => ({
            ...base,
            maxHeight: minHeight38 ? "140px" : '170px',
            borderRadius: "5px",
            borderColor: '#ffebd9',
            fontSize: '14px'
        })
        // indicatorSeparator: state => ({
        //     display: 'none',
        // }),
        // indicatorsContainer: (provided, state) => ({
        //     ...provided,
        //     height: '30px',
        // }),
    };


    const [selectedOption, setSelectedOption] = useState(null);

    const handleSelectChange = (selected) => {
        if (selected) {
            setSelectedOption(selected)
            props.onChange(selected);
        }
    };



    return (
        <div>
            <Select
                isSearchable
                options={list}
                value={value}
                isDisabled={isDisabled === true ? true : false}
                placeholder={placeholder}
                theme={(theme) => ({
                    ...theme,
                    borderRadius: '5px',
                    height: '50px',
                    borderColor: '#ffebd9',
                    colors: {
                        ...theme.colors,
                        primary25: 'white',
                        primary: '#ffebd9',
                    },
                    background: "white",
                })}
                styles={customStyles}
                className={selectedOption ? styles.selectedOption : ''}
                onChange={(item) => handleSelectChange(item)}
                components={{
                    Option: props => (
                        <CustomSelectOptions
                            items={props}
                            value={selectedOption}
                        />
                    )
                }}
            />
        </div>
    )
}