import React, { useRef } from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css'; // Import Quill's CSS for styling
import '../../../public/scss/pages/textEditor.css';


function TextEditor(props) {
    const { data, onChange } = props;
    let quillRef = useRef();


    const handleChange = (value) => {
        onChange(value);
    };


    const handleColorChange = (e) => {
        const selectedColor = e.target.value;
        const quill = quillRef.getEditor();
        quill.format('color', selectedColor);
    };


    return (
        <div>
            <input
                style={{ display: 'none' }}
                type="color"
                onChange={handleColorChange}
            />
            <ReactQuill
                ref={quillRef}
                value={data}
                onChange={handleChange}
                modules={TextEditor.modules}
                formats={TextEditor.formats}
                theme="snow"
            />
        </div>
    );
}

TextEditor.modules = {
    toolbar: [
        [{ 'header': '1' }, { 'header': '2' }],
        ['bold', 'italic', 'underline', 'strike'],
        [{ 'list': 'ordered' }, { 'list': 'bullet' }],
        ['link', 'image'],
        ['clean'],
        [{ 'color': ["red", "green", "yellow", "blue", "#11C081", "#1871e7", "#8BB612", "#FF8717", "#818181", "#FFB057", "#DDF0FB", "#00954D", "#8088b6", "#30C5EC", "#2FC6EC", "#4B5782"] }],
    ],
};

TextEditor.formats = [
    'header',
    'bold', 'italic', 'underline', 'strike',
    'list', 'bullet',
    'link', 'image', 'color',
];

export default TextEditor;
