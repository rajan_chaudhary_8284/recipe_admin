import React, { useRef, useState } from "react";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import styles from '../../public/scss/pages/datepicker.module.scss';
import '../../public/scss/pages/datepicker.css';
import { faCalendarDays, } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { dateFormat2 } from "./General";
// import 'react-datepicker/dist/react-datepicker.css';

function Datepicker(props) {
    const {
        datePickerCustomStyle,
        date,
        setDate,
        iconStyleMain,
        iconStyleSub,
        calenderStyle,
        minDate
    } = props

    const [startDate, setStartDate] = useState(new Date());
    const [activeIcon, setActiveIcon] = useState(false);
    const datepickerRef = useRef(null);


    function handleClickDatepickerIcon() {
        const datepickerElement = datepickerRef.current;
        datepickerElement.setFocus(true);
    };


    const handleChange = (date) => {
        if (setDate) {
            setDate(date)
        }
        else;
    };

    return (
        <div className={styles.main}>
            <div className={iconStyleMain ? iconStyleMain : styles.iconView} onClick={() => handleClickDatepickerIcon()}>
                <FontAwesomeIcon
                    icon={faCalendarDays}
                    className={iconStyleSub ? iconStyleSub : (activeIcon ? styles.activeIcon : styles.icon)}
                />
            </div>
            <DatePicker
                onFocus={() => setActiveIcon(true)}
                onBlur={() => setActiveIcon(false)}
                ref={datepickerRef}
                className={datePickerCustomStyle ? datePickerCustomStyle : styles.dateinput}
                selected={date}
                onChange={(date) => handleChange(date)}
                dateFormat="dd/MM/yyyy"
                placeholderText="DD/MM/YYYY"
                showIcon={true}
                calendarClassName={calenderStyle ? calenderStyle : styles.calander}
                popperClassName={styles.popperContainer}
                popperPlacement={"bottom-start"}
                wrapperClassName={styles.wrapperClass}
                minDate={minDate ? minDate : null}
            />
        </div>
    );
}

export default Datepicker;
