import React, { useRef, useState } from "react";
import { ProgressBar } from "react-bootstrap";
import { renderImage } from "./General";
import { faImage } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
// import ImageUploadController from "@/app/apis/controllers/imageController";
// import WebConstant from "@/app/apis/webConstants";
import userImage from '../../public/images/userDummy.png';
import styles from '../../public/scss/pages/uploadImage.module.scss';
import Image from "next/image";

function UploadImageBox(props) {
    const inputFile = useRef(null);
    const [isLoading, setLoader] = useState(false);
    const [progress, setProgress] = React.useState(0);
    const [isError, setIsError] = useState(null);

    const handleImage = async (value) => {
        setProgress(0);
        if (["image/jpeg", "image/png", "image/gif", "image/webp", "image/jfif", "application/pdf",].includes(value.type)) {
            if (value.size < WebConstant.maxFileSize) {
                setIsError(null);
                if (isLoading === false) {
                    setLoader(true);
                    setProgress(0.5);
                    let callback = (p) => {
                        // setProgress(p > 100 ? p * 1 - 5 : p);
                        setProgress(p > 100 ? 100 : p);
                    };
                    let response = await ImageUploadController.uploadImage(value, callback);
                    setLoader(false);
                    if (response && response.status) {
                        setProgress(Math.round(progress * 100));
                        setProgress(0);
                        props.onResponse(response);
                    } else {
                        setProgress(0);
                        setIsError(response.message);
                    }
                }
            } else {
                setIsError("File size exceeds the maximum limit of 5MB.");
            }
        } else {
            setIsError(
                "Please upload file having extension jpg, jpeg, png, gif, webp or pdf."
            );
        }
    };

    const [imageFallback, setImageFallback] = useState(false);

    //------------ This function is called when the image fails to load -----------
    const handleImageError = () => {
        // You can set a state variable to indicate that the image has failed.
        setImageFallback(true);
    };

    return (
        <>
            <div className={styles.main}>
                {/* <img src={props.image && props.image.small ? renderImage(props.image.small) : renderNoAvatar(props.name)} /> */}
                <div className={styles.imageIconMain}>
                    <div className={styles.img_area}>

                        {/* {!imageFallback && ( */}
                        <Image
                            src={
                                props && props.image
                                    ? renderImage(props.image)
                                    : userImage
                            }
                            className={styles.image}
                            height={200}
                            width={200}
                            style={{ objectFit: 'cover' }}
                            alt="..."
                            priority={true}
                            loading='eager'
                        // onError={handleImageError}
                        />
                        {/* )} */}

                        {/* {imageFallback && (
                            <Image
                                src={userImage}
                                alt="Fallback Image"
                                className={styles.image}
                                height={200}
                                width={200}
                                style={{ objectFit: 'cover' }}
                            />
                        )} */}
                    </div>

                    {props && props.hideIcon == 2 ?
                        null :
                        <div className={styles.iconMain}>
                            <div className={styles.editImageMain}
                                onClick={() => inputFile.current.click()}
                            >
                                <FontAwesomeIcon icon={faImage}
                                    className={styles.icon}
                                />
                            </div>
                        </div>
                    }

                </div>

                {/* {
                    !isLoading
                    &&
                    <div className="edits_btn">
                        <ul>
                            <li>
                                <a href="" onClick={(e) => {
                                    e.preventDefault();
                                    fileInput.current.click()
                                }}><i class="fal fa-pen"></i></a>
                            </li>
                            {
                                props.image
                                &&
                                <li>
                                    <a href="" onClick={async (e) => {
                                            e.preventDefault();
                                                setLoader(true);
                                                await props.removeImage()
                                                setLoader(false);
                                        }}><i class="fal fa-times"></i></a>
                                </li>
                            }
                        </ul>
                    </div>
                } */}
                <input
                    style={{ display: "none" }}
                    ref={inputFile}
                    id="photo2"
                    type="file"
                    onChange={(event) => {
                        handleImage(event.target.files[0]);
                        event.target.value = "";
                    }}
                    accept="image/*"
                />
                {/* {progress !== 0 ? (
                    <ProgressBar now={progress} variant="danger" label={`${progress}%`} max={100} animated/>
                ) : null} */}
                {isLoading && progress !== 0 ? (
                    <div className={styles.progressbar}>
                        <ProgressBar
                            now={progress}
                            variant="primary"
                            label={`${progress}%`}
                            max={100}
                            animated
                            style={{ height: '7px', width: '150px', marginBlock: '10px' }}
                        />
                    </div>
                ) : null}
                <center>
                    <small className="text-danger">{isError ? isError : ``}</small>
                </center>
            </div>
        </>
    );
};
export default UploadImageBox;
